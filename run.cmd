SET BIN_PATH=bin\win32
IF %PROCESSOR_ARCHITECTURE%==AMD64 SET BIN_PATH=bin\x64

SET SCENE_PATH=data/crysponza_bubbles/sponza.obj

%BIN_PATH%\clustered_forward_demo_Release_CUDA.exe %SCENE_PATH%
REM Use the below command line if you experience CUDA related problems running the demo.
REM %BIN_PATH%\clustered_forward_demo_Release.exe %SCENE_PATH%

pause