/*********************************************************** -- HEAD -{{{1- */
/** Light Assignment implementation
 */
/******************************************************************* -}}}1- */

#include <float.h>

//--//////////////////////////////////////////////////////////////////////////
//--    $ helpers                   ///{{{1///////////////////////////////////
namespace
{
	__device__ inline bool overlaps(const float3 aabbMin, const float3 aabbMax, float4 sphere)
	{
		float dmin = 0.0f;

#		define LOOP_BODY(_suffix_) \
		{ \
			if( sphere._suffix_ < aabbMin._suffix_ )  \
			{ \
				dmin += square(sphere._suffix_ - aabbMin._suffix_ );  \
			} \
			else if( sphere._suffix_ > aabbMax._suffix_ )  \
			{ \
				dmin += square( sphere._suffix_ - aabbMax._suffix_ );  \
			} \
		}
		LOOP_BODY(x);
		LOOP_BODY(y);
		LOOP_BODY(z);
#		undef LOOP_BODY

		return dmin <= square(sphere.w);
	} 
}


//--    LH: helpers                 ///{{{1///////////////////////////////////
namespace
{
	// operators
	struct AabbMinOp
	{
		__device__ __host__ float3 operator() (float3 aA, float3 aB) const
		{
			return make_float3( min(aA.x,aB.x), min(aA.y,aB.y), min(aA.z,aB.z) );
		}
		__device__ __host__ float3 identity() const
		{
			return make_float3( +FLT_MAX, +FLT_MAX, +FLT_MAX );
		}
	};
	struct AabbMaxOp
	{
		__device__ __host__ float3 operator() (float3 aA, float3 aB) const
		{
			return make_float3( max(aA.x,aB.x), max(aA.y,aB.y), max(aA.z,aB.z) );
		}
		__device__ __host__ float3 identity() const
		{
			return make_float3( -FLT_MAX, -FLT_MAX, -FLT_MAX );
		}
	};

	// overlap test
	inline __device__ bool overlaps( const float3& aAMin, const float3& aAMax, const float3& aBMin, const float3& aBMax ) 
	{
		return aAMax.x > aBMin.x && aAMin.x < aBMax.x
			&& aAMax.y > aBMin.y && aAMin.y < aBMax.y
			&& aAMax.z > aBMin.z && aAMin.z < aBMax.z; // See Aabb.h
	}
}

//--    light_hierarchy_*           ///{{{1///////////////////////////////////
template <class tLightData>
__global__ void light_hierarchy_make_keys( const ::float4* __restrict__ aabbMin, const ::float4* __restrict__ aabbMax, const tLightData aLightData, uint32_t* aSortKeys, uint32_t* aLightIndices )
{
	unsigned index = threadIdx.x + blockIdx.x*blockDim.x;

	if( index >= aLightData.light_count() )
		return;

	const unsigned kCoordBits = 8;
	const unsigned kCoordScale = (1u<<kCoordBits)-1u;

	float4 sphere = aLightData.get_light_range( index );

	float3 rng = make_float3(*aabbMax) - make_float3(*aabbMin);
	float3 pos = make_float3(sphere) - make_float3(*aabbMin);

	//assert( pos.x >= 0.0f && pos.y >= 0.0f && pos.z >= 0.0f );
	//assert( pos.x <= rng.x && pos.y <= rng.y && pos.z <= rng.z );

	float3 norm = pos / rng;
	uint32_t key = spreadBits<kCoordBits>( uint32_t(norm.x * kCoordScale), 3, 0 )
	             | spreadBits<kCoordBits>( uint32_t(norm.y * kCoordScale), 3, 1 )
	             | spreadBits<kCoordBits>( uint32_t(norm.z * kCoordScale), 3, 2 );


	aSortKeys[index] = key;
	aLightIndices[index] = index;
}

template< class tLightData, class tHierarchy >
__global__ void __launch_bounds__(32*6,6) light_hierarchy_build_from_leaves( const tLightData aLightData, const tHierarchy aHierarchy, const uint32_t* aLightIndexTable, const float4* aLightAABBMin, const float4* aLightAABBMax )
{
	__shared__ union Shared_
	{
		float3 reduceBuffer[32+32/2];
	} shared_[6];
	Shared_& shared = shared_[threadIdx.y];

	const unsigned warpId = threadIdx.y + blockIdx.x * blockDim.y;
	if( aHierarchy.get_level_start(aHierarchy.get_num_levels()-2)+warpId >= aHierarchy.get_num_nodes() ) 
		return;

	const unsigned leafIndex = warpId * blockDim.x + threadIdx.x;

	unsigned lightIndex;
	float3 lightMin, lightMax;

	if( leafIndex < aLightData.light_count() )
	{
		lightIndex = aLightIndexTable[leafIndex];
		float4 range = aLightData.get_light_range( lightIndex );
		
		lightMin = make_float3( range.x-range.w, range.y-range.w, range.z-range.w );
		lightMax = make_float3( range.x+range.w, range.y+range.w, range.z+range.w );
	}
	else
	{
		lightIndex = ~0u;

		lightMin = make_float3( +FLT_MAX, +FLT_MAX, +FLT_MAX );
		lightMax = make_float3( -FLT_MAX, -FLT_MAX, -FLT_MAX );
	}

	typedef chag::pp::KernelSetupWarp<> WarpSetup;
	typedef chag::pp::Unit<WarpSetup> WarpUnit;


	float3 nodeMin = WarpUnit::reduce( lightMin, AabbMinOp(), shared.reduceBuffer );
	float3 nodeMax = WarpUnit::reduce( lightMax, AabbMaxOp(), shared.reduceBuffer );

	aHierarchy.set_leaf( leafIndex, lightIndex ); // @ level m_numLevels-1
	aHierarchy.set_node( aHierarchy.get_num_levels()-2, warpId, nodeMin, nodeMax );

	// Hack: setup root node once
	if( threadIdx.x == 0 && threadIdx.y == 0 && blockIdx.x == 0 )
	{
		aHierarchy.set_node( 0, 0, make_float3(*aLightAABBMin), make_float3(*aLightAABBMax) );
	}
}

template< class tHierarchy >
__global__ void __launch_bounds__(32*6,6) light_hierarchy_build_upper( const tHierarchy aHierarchy, unsigned aFromLevel )
{
	//assert( aFromLevel > 1 && aFromLevel <= aHierarchy.get_num_levels()-2 );

	__shared__ union Shared_
	{
		float3 reduceBuffer[32+32/2];
	} shared_[6];
	Shared_& shared = shared_[threadIdx.y];

	const unsigned warpId = threadIdx.y + blockIdx.x * blockDim.y;
	//assert( warpId < aHierarchy.get_num_nodes() );

	if( warpId >= aHierarchy.get_level_nodes(aFromLevel-1) )
		return;

	const unsigned childIndex = aHierarchy.get_level_start(aFromLevel) + warpId * blockDim.x + threadIdx.x;

	float3 childMin, childMax;

	if( childIndex < aHierarchy.get_num_nodes() )
	{
		childMin = aHierarchy.get_node_min( childIndex );
		childMax = aHierarchy.get_node_max( childIndex );
	}
	else
	{
		childMin = make_float3( +FLT_MAX, +FLT_MAX, +FLT_MAX );
		childMax = make_float3( -FLT_MAX, -FLT_MAX, -FLT_MAX );
	}

	typedef chag::pp::KernelSetupWarp<> WarpSetup;
	typedef chag::pp::Unit<WarpSetup> WarpUnit;


	float3 parentMin = WarpUnit::reduce( childMin, AabbMinOp(), shared.reduceBuffer );
	float3 parentMax = WarpUnit::reduce( childMax, AabbMaxOp(), shared.reduceBuffer );
	
	aHierarchy.set_node( aFromLevel-1, warpId, parentMin, parentMax );
}


template< class ClusterAabbBuilder, class tLightData, class tHierarchy >
__global__ void __launch_bounds__(32*6,6) light_hierarchy_assign_count( const tLightData aLightData, const tHierarchy aHierarchy, const typename ClusterAabbBuilder clusterAabbBuilder, const uint32_t* __restrict__ clusterKeys, unsigned aNumClusters, uint32_t* aClusterLightCounts )
{
	typedef chag::pp::Unit< chag::pp::KernelSetupWarp<1,6*32> > WarpUnit;
	typedef chag::pp::op::Add<unsigned> AddOp;

	__shared__ union SharedMem_
	{
		unsigned reduceBuffer[32+32/2];
	} shared_[6];

	SharedMem_& shared = shared_[threadIdx.y];

	// find cluster
	unsigned clusterIdx = threadIdx.y + blockIdx.x * blockDim.y;
	if( clusterIdx >= aNumClusters )
		return;

	float3 aabbMin;
	float3 aabbMax;
	clusterAabbBuilder.getAabb(clusterKeys[clusterIdx], aabbMin, aabbMax);

	// count light overlaps
	unsigned count = aHierarchy.count_light_overlaps(aLightData, aabbMin, aabbMax);
	unsigned totalCount = WarpUnit::reduce( count, AddOp(), shared.reduceBuffer );

	// store total count
	if( 0 == threadIdx.x )
	{
		aClusterLightCounts[clusterIdx] = totalCount;
	}
}

__global__ void light_hierarchy_assign_scan( const uint32_t* aClusterLightCounts, unsigned aNumClusters, unsigned aBufferSize, uint32_t* aClusterLightOffsets )
{
	typedef chag::pp::KernelSetupBlock<1,1024> KernelSetup1024;
	typedef chag::pp::op::Add<uint32_t> AddOp;
	typedef chag::pp::Range<uint32_t,KernelSetup1024> Range;

	//assert( aBufferSize && aBufferSize/1024*1024 == aBufferSize );

	__shared__ union
	{
		uint32_t prefixBuffer[1024+1024/2];
	} shared;

	uint32_t total = Range::prefix( aClusterLightCounts, aClusterLightCounts+aBufferSize, aClusterLightOffsets, 0u, AddOp(), shared.prefixBuffer );
}

template< class ClusterAabbBuilder, class tLightData, class tHierarchy >
__global__ void __launch_bounds__(32*6,6) light_hierarchy_assign_store( const tLightData aLightData, const tHierarchy aHierarchy, const typename ClusterAabbBuilder clusterAabbBuilder, const uint32_t* __restrict__ clusterKeys, const uint32_t* aClusterLightOffsets, unsigned aNumClusters, uint32_t* aClusterLightIndices )
{
	typedef chag::pp::Unit< chag::pp::KernelSetupWarp<1,6*32> > WarpUnit;
	typedef chag::pp::op::Add<unsigned> AddOp;

	// find cluster
	unsigned clusterIdx = threadIdx.y + blockIdx.x * blockDim.y;
	if( clusterIdx >= aNumClusters )
		return;
 
	// and get its bounding box
	float3 aabbMin;
	float3 aabbMax;
	clusterAabbBuilder.getAabb(clusterKeys[clusterIdx], aabbMin, aabbMax);


	// store light overlaps
	unsigned clusterOffset = aClusterLightOffsets[clusterIdx];

	aHierarchy.store_light_overlaps( aLightData, aabbMin, aabbMax, clusterOffset, aClusterLightIndices );
}

//--    LightHierarchy32 - Helpers  ///{{{1///////////////////////////////////
template< unsigned tLevel, unsigned tMaxLevel >
struct LhInvokeCount
{
	template< class tLightData >
	static __forceinline__ __device__ unsigned inv( const LightHierarchy32& aLH, const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, unsigned aIndex )
	{
		return aLH.count_lo_upper_<tLevel,tMaxLevel>( aLightData, aabbMin, aabbMax, aIndex );
	}
};
template< unsigned tLevel > struct LhInvokeCount<tLevel,tLevel> {
	template< class tLightData >
	static __forceinline__ __device__ unsigned inv( const LightHierarchy32& aLH, const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, unsigned aIndex )
	{
		return aLH.count_lo_leaves_<tLevel>( aLightData, aabbMin, aabbMax, aIndex );
	}
};
template< unsigned tLevel, unsigned tMaxLevel >
struct LhInvokeStore
{
	template< class tLightData >
	static __forceinline__ __device__ unsigned inv( const LightHierarchy32& aLH, const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, unsigned aIndex, unsigned aClusterOffset, uint32_t* aClusterLightIndices )
	{
		return aLH.store_lo_upper_<tLevel,tMaxLevel>( aLightData, aabbMin, aabbMax, aIndex, aClusterOffset, aClusterLightIndices );
	}
};
template< unsigned tLevel > struct LhInvokeStore<tLevel,tLevel> {
	template< class tLightData >
	static __forceinline__ __device__ unsigned inv( const LightHierarchy32& aLH, const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, unsigned aIndex, unsigned aClusterOffset, uint32_t* aClusterLightIndices )
	{
		return aLH.store_lo_leaves_<tLevel>( aLightData, aabbMin, aabbMax, aIndex, aClusterOffset, aClusterLightIndices );
	}
};

//--    LightHierarchy32            ///{{{1///////////////////////////////////
inline LightHierarchy32::LightHierarchy32()
{
	m_lightIndices = 0;
	m_nodesMin = m_nodesMax = 0;
	m_numMaxNodes = m_numMaxLights = 0;
}
/*
inline LightHierarchy32::~LightHierarchy32()
{
}*/

	// cpu
inline __host__ void LightHierarchy32::set_light_count( unsigned aNumLights )
{
	m_numLevels = 1;
	m_numLights = aNumLights;

	// compute number of levels
	// Ok, this is probably some log(), but by brain hurts already
	unsigned n = aNumLights;

	do
	{
		n = (n+32u-1u)/32u;
		++m_numLevels;
	}
	while( n > 1u );

	// compute number of nodes required (full tree)
	// leaf nodes do not need nodes associated with them
	m_numNodes = (uint32_t(powf( float(32u), float(m_numLevels - 2u))) - 1u) / (32u-1u) + (aNumLights + 32u - 1u)/32u;

	// resize buffers
	if( m_numNodes > m_numMaxNodes )
	{
		if( m_nodesMin ) cudaFree( m_nodesMin );
		if( m_nodesMax ) cudaFree( m_nodesMax );

		cudaMalloc( (void**)&m_nodesMin, sizeof(float4)*m_numNodes );
		cudaMalloc( (void**)&m_nodesMax, sizeof(float4)*m_numNodes );

		m_numMaxNodes = m_numNodes;
	}

	if( m_numLights > m_numMaxLights )
	{
		if( m_lightIndices ) cudaFree( m_lightIndices );
		cudaMalloc( (void**)&m_lightIndices, sizeof(uint32_t)*m_numLights );

		m_numMaxLights = m_numLights;
	}
}
inline __host__ void LightHierarchy32::get_node_aabbs( float4* aNodeMins, float4* aNodeMaxs ) const
{
	//assert( aNodeMins && aNodeMaxs );
	cudaMemcpy( aNodeMins, m_nodesMin, sizeof(float4)*m_numNodes, cudaMemcpyDeviceToHost );
	cudaMemcpy( aNodeMaxs, m_nodesMax, sizeof(float4)*m_numNodes, cudaMemcpyDeviceToHost );
}

	// cpu+gpu
inline __device__ __host__ unsigned LightHierarchy32::get_num_nodes() const
{
	return m_numNodes;
}
inline __device__ __host__ unsigned LightHierarchy32::get_num_levels() const
{
	return m_numLevels;
}
inline __device__ __host__ unsigned LightHierarchy32::get_level_start( unsigned aLvl ) const
{
	//assert( aLvl < m_numLevels );

#	if 1
	switch( aLvl )
	{
		case 0: return 0;
		case 1: return 1;
		case 2: return 33;
		case 3: return 1057;
		case 4: return 33825;
		case 5: return 1082401;
		//case 6: return 34636833; // probably not needed
	}
	
	//assert(0);
	return 0;
#	else
	return (pow( 32u, aLvl+1u-1u )-1u) / (32u-1u);
#	endif
}
inline __device__ __host__ unsigned LightHierarchy32::get_level_nodes( unsigned aLvl ) const
{
	//assert( aLvl < m_numLevels );

	switch( aLvl )
	{
		case 0: return 1;
		case 1: return 32;
		case 2: return 1024;
		case 3: return 32*1024;
		case 4: return 1024*1024;
		case 5: return 32*1024*1024;
		//case 6: return 1024*1024*1024; // probably not needed
	}
	
	//assert(0);
	return 0;
}

	// gpu
inline __device__ void LightHierarchy32::set_root( const float4* aRootMin, const float4* aRootMax ) const
{
	*m_nodesMin = *aRootMin;
	*m_nodesMax = *aRootMax;
}
inline __device__ void LightHierarchy32::set_node( unsigned aLevel, unsigned aNodeId, const float3& aNodeMin, const float3& aNodeMax ) const
{
	unsigned levelStart = get_level_start(aLevel);
	unsigned nodeIndex = levelStart + aNodeId;

	//assert( nodeIndex < m_numNodes );

	if( 0 == threadIdx.x )
	{
		m_nodesMin[nodeIndex] = make_float4( aNodeMin, 0.0f );
		m_nodesMax[nodeIndex] = make_float4( aNodeMax, 0.0f );
	}
}
inline __device__ void LightHierarchy32::set_leaf( unsigned aLeafId, unsigned aLightIndex ) const
{
	if( aLeafId < m_numLights )
	{
		m_lightIndices[aLeafId] = aLightIndex;
	}
}

inline __device__ float3 LightHierarchy32::get_node_min( unsigned aIndex ) const
{
	//assert( aIndex < m_numNodes );
	return make_float3(m_nodesMin[aIndex]);
}
inline __device__ float3 LightHierarchy32::get_node_max( unsigned aIndex ) const
{
	//assert( aIndex < m_numNodes );
	return make_float3(m_nodesMax[aIndex]);
}

template< class tLightData>
inline __device__ unsigned LightHierarchy32::count_light_overlaps( const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax) const
{
	// test against root node
	if( !overlaps( aabbMin, aabbMax, get_node_min(0), get_node_max(0) ) )
	{
		return 0;
	}

	// start recursion
	if( m_numLevels == 2 )
	{
		return LhInvokeCount<1,1>::inv( *this, aLightData, aabbMin, aabbMax, 1 );
	}
	else if( m_numLevels == 3 )
	{
		return LhInvokeCount<1,2>::inv( *this, aLightData, aabbMin, aabbMax, 1 );
	}
	else if( m_numLevels == 4 )
	{
		return LhInvokeCount<1,3>::inv( *this, aLightData, aabbMin, aabbMax, 1 );
	}
	else if( m_numLevels == 5 )
	{
		return LhInvokeCount<1,4>::inv( *this, aLightData, aabbMin, aabbMax, 1 );
	}

	//assert(0);
	return 0;
}

template< class tLightData >
inline __device__ unsigned LightHierarchy32::store_light_overlaps( const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, unsigned aClusterOffset, uint32_t* aClusterLightIndices ) const
{
	// test against root node
	if( !overlaps( aabbMin, aabbMax, get_node_min(0), get_node_max(0) ) )
	{
		return aClusterOffset;
	}

	// start recursion
	if( m_numLevels == 2 )
	{
		return LhInvokeStore<1,1>::inv( *this, aLightData, aabbMin, aabbMax, 1, aClusterOffset, aClusterLightIndices );
	}
	else if( m_numLevels == 3 )
	{
		return LhInvokeStore<1,2>::inv( *this, aLightData, aabbMin, aabbMax, 1, aClusterOffset, aClusterLightIndices );
	}
	else if( m_numLevels == 4 )
	{
		return LhInvokeStore<1,3>::inv( *this, aLightData, aabbMin, aabbMax, 1, aClusterOffset, aClusterLightIndices );
	}
	else if( m_numLevels == 5 )
	{
		return LhInvokeStore<1,4>::inv( *this, aLightData, aabbMin, aabbMax, 1, aClusterOffset, aClusterLightIndices );
	}

	//assert(0);
	return 0;
}

template< unsigned tLevel, unsigned tMaxLevel, class tLightData >
inline __device__ unsigned LightHierarchy32::count_lo_upper_( const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, unsigned aIndex ) const
{
	// continue traversal
	float3 nodeMin, nodeMax;
	if( aIndex+threadIdx.x < m_numNodes )
	{
		nodeMin = get_node_min( aIndex + threadIdx.x );
		nodeMax = get_node_max( aIndex + threadIdx.x );
	}
	else
	{
		nodeMin = make_float3( +FLT_MAX, +FLT_MAX, +FLT_MAX );
		nodeMax = make_float3( -FLT_MAX, -FLT_MAX, -FLT_MAX );
	}

	bool overlap = overlaps( aabbMin, aabbMax, nodeMin, nodeMax );
	//assert( !overlap || (aIndex+threadIdx.x < m_numNodes) );
	
	unsigned result = __ballot( overlap );

	unsigned count = 0;
	while( __clz(result) != 32 )
	{
		unsigned child = 31 - __clz(result);
		count += LhInvokeCount<tLevel+1,tMaxLevel>::inv( *this, aLightData, aabbMin, aabbMax, (aIndex+child)*32 + 1 );

		result = result & ~(1u<<child);
	}

	return count;
}
template< unsigned tLevel, class tLightData >
inline __device__ unsigned LightHierarchy32::count_lo_leaves_( const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, unsigned aIndex ) const
{
	unsigned leafIndex = aIndex + threadIdx.x - get_level_start(tLevel);
	if( leafIndex >= m_numLights )
		return 0;

	unsigned lightIndex = m_lightIndices[leafIndex];
	//assert( lightIndex < aLightData.light_count() ) ;

	float4 sphere = aLightData.get_light_range( lightIndex );
	if (overlaps(aabbMin, aabbMax, sphere))
		return 1;

	return 0;
}

template< unsigned tLevel, unsigned tMaxLevel, class tLightData >
inline __device__ unsigned LightHierarchy32::store_lo_upper_( const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, unsigned aIndex, unsigned aClusterOffset, uint32_t* aClusterLightIndices ) const
{
	// continue traversal
	float3 nodeMin, nodeMax;
	if( aIndex+threadIdx.x < m_numNodes )
	{
		nodeMin = get_node_min( aIndex + threadIdx.x );
		nodeMax = get_node_max( aIndex + threadIdx.x );
	}
	else
	{
		nodeMin = make_float3( +FLT_MAX, +FLT_MAX, +FLT_MAX );
		nodeMax = make_float3( -FLT_MAX, -FLT_MAX, -FLT_MAX );
	}

	bool overlap = overlaps( aabbMin, aabbMax, nodeMin, nodeMax );
	//assert( !overlap || (aIndex+threadIdx.x < m_numNodes) );
	
	unsigned result = __ballot( overlap );

	unsigned offset = aClusterOffset;
	while( __clz(result) != 32 )
	{
		unsigned child = 31 - __clz(result);
		offset = LhInvokeStore<tLevel+1,tMaxLevel>::inv( *this, aLightData, aabbMin, aabbMax, (aIndex+child)*32 + 1, offset, aClusterLightIndices );

		result = result & ~(1u<<child);
	}

	return offset;
}
template< unsigned tLevel, class tLightData >
inline __device__ unsigned LightHierarchy32::store_lo_leaves_( const tLightData& aLightData, const float3 aabbMin, const float3 aabbMax, unsigned aIndex, unsigned aClusterOffset, uint32_t* aClusterLightIndices ) const
{
	unsigned leafIndex = aIndex + threadIdx.x - get_level_start(tLevel);
	unsigned lightIndex;
	
	bool isect = false;

	if( leafIndex < m_numLights )
	{
		lightIndex = m_lightIndices[leafIndex];
		//assert( lightIndex < aLightData.light_count() );

		float4 sphere = aLightData.get_light_range( lightIndex );
		isect = overlaps(aabbMin, aabbMax, sphere);
	}

	uint32_t ballot = __ballot( isect );
	uint32_t masked;
	asm(
		"{\n\t"
			".reg .u32 t0;\n\t"
			"mov.u32 t0, %%lanemask_lt;\n\t"
			"and.b32 %0, %1, t0;\n\t"
		"}\n\t"
		: "=r"(masked)
		: "r"(ballot)
	);

	uint32_t offset = __popc( masked );

	if( isect )
	{
		aClusterLightIndices[aClusterOffset+offset] = lightIndex;
	}

	return aClusterOffset +  __popc(ballot);
}

//--///}}}1////////////// vim:syntax=cuda:foldmethod=marker:ts=4:noexpandtab:
