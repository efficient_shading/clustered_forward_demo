#include "Config.h"
#include "linmath/int2.h"
#include "linmath/float4.h"
#include "Globals.h"
#include "utils/GlBufferObject.h"

/**
 * This contains all cuda implementations for tiling/clustering and shading. It should exist in a single instance
 * and only be created if CUDA is present on the system, if not certain algorithms will not work or fall back to
 * CPU versions.
 * Supports:
 *  Build light clusters for clustered forward.
 */
class CudaRenderer
{
public:

	virtual void init(GlBufferObject<uint32_t> &fullClusterBuffer, GlBufferObject<chag::uint2> &fullClusterGridBuffer, 
		GlBufferObject<chag::float4> &lightPositionRangeBuffer, GlBufferObject<chag::float4> &lightColorBuffer, GlBufferObject<int> &tileLightIndexListsBuffer) = 0;

	/**
	 * Assumes lights are in buffers already...
	 */
	virtual void buildClusters(const Globals &globals, uint32_t numLights) = 0;

	static CudaRenderer *create();
};