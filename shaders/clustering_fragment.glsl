#version 330 compatibility
/****************************************************************************/
/* Copyright (c) 2011, Ola Olsson, Ulf Assarsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#include "globals.glsl"
#include "clusteredShading.glsl"
#include "ObjModel.glsl"

#extension GL_ARB_shader_image_load_store : enable

// this enables early depth test etc, even though there are side effects.
layout(early_fragment_tests) in;

in vec3 v2f_viewSpacePos;
in vec2 v2f_texCoord;

out vec4 fragmentColor;

layout(r32ui) uniform uimageBuffer fullClusterImage;

/**
 * This shader makes use of a side effect (imageStore) to set a flag in the grid cell 
 * corresponding to the cluster. By drawing all transparent geometry all clusters
 * that contain any samples are flagged as non-zero. This pre pass does not need to 
 * actually write to the color buffer.
 */
void main() 
{
#if ENABLE_ALPHA_TEST
	// Manual alpha test (note: alpha test is no longer part of Opengl 3.3).
	if (texture2D(opacity_texture, v2f_texCoord).r < 0.5)
	{
		discard;
	}
#endif // ENABLE_ALPHA_TEST

	imageStore(fullClusterImage, calcClusterOffset(gl_FragCoord.xy, v2f_viewSpacePos.z), uvec4(1U));
	//One might want to pack bits to save memory... 
	// imageAtomicOr(fullClusterImage, calcClusterBitOffset(gl_FragCoord.xy, v2f_viewSpacePos.z), 1U);
	fragmentColor = vec4(1.0);
}
