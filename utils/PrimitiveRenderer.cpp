#include "PrimitiveRenderer.h"
#include <GL/glew.h>
#include <linmath/float4x4.h>
#include <utils/SimpleShader.h>
#include <vector>

namespace chag
{


void PrimitiveRenderer::init()
{
	m_transform = chag::make_identity<float4x4>();

	createConeMesh();
	createSphereModel();
	buildAabb();
	buildPlane();
	buildQuadGrid();
}

const int g_numSphereSubdivs = 2;
#define POW4(x) (1 << (2 * (x)))
#define SPHERE_SIZE (8 * 3 * POW4(g_numSphereSubdivs))


static void subDivide(chag::float3 *&dest, const chag::float3 &v0, const chag::float3 &v1, const chag::float3 &v2, int level)
{
	if (level)
	{
		chag::float3 v3 = normalize(v0 + v1);
		chag::float3 v4 = normalize(v1 + v2);
		chag::float3 v5 = normalize(v2 + v0);

		subDivide(dest, v0, v3, v5, level - 1);
		subDivide(dest, v3, v4, v5, level - 1);
		subDivide(dest, v3, v1, v4, level - 1);
		subDivide(dest, v5, v4, v2, level - 1);
	}
	else
	{
		*dest++ = v0;
		*dest++ = v1;
		*dest++ = v2;
	}
}

void PrimitiveRenderer::createSphereModel()
{
	chag::float3 sphere[SPHERE_SIZE];
	chag::float3 *dest = sphere;

	subDivide(dest, chag::make_vector<float>(0, 1, 0), chag::make_vector<float>(0, 0, 1), chag::make_vector<float>(1, 0, 0), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, 1, 0), chag::make_vector<float>(1, 0, 0), chag::make_vector<float>(0, 0, -1), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, 1, 0), chag::make_vector<float>(0, 0, -1), chag::make_vector<float>(-1, 0, 0), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, 1, 0), chag::make_vector<float>(-1, 0, 0), chag::make_vector<float>(0, 0, 1), g_numSphereSubdivs);

	subDivide(dest, chag::make_vector<float>(0, -1, 0), chag::make_vector<float>(1, 0, 0), chag::make_vector<float>(0, 0, 1), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, -1, 0), chag::make_vector<float>(0, 0, 1), chag::make_vector<float>(-1, 0, 0), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, -1, 0), chag::make_vector<float>(-1, 0, 0), chag::make_vector<float>(0, 0, -1), g_numSphereSubdivs);
	subDivide(dest, chag::make_vector<float>(0, -1, 0), chag::make_vector<float>(0, 0, -1), chag::make_vector<float>(1, 0, 0), g_numSphereSubdivs);

	g_sphereVertexBuffer.init(SPHERE_SIZE, sphere);

	glGenVertexArrays(1, &m_sphereVaob);
	glBindVertexArray(m_sphereVaob);

	g_sphereVertexBuffer.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	g_sphereVertexBuffer.unbind();
	glBindVertexArray(0);
}


void PrimitiveRenderer::drawSphere(const float3 &pos, float size, SimpleShader *shader, bool wireFrame)
{
	float4x4 tfm = make_translation(pos) * make_scale<float4x4>(size);
	shader->setUniform("modelTransform", m_transform * tfm);
	glBindVertexArray(m_sphereVaob);
	glDrawArrays(wireFrame ? GL_LINE_LOOP : GL_TRIANGLES, 0, SPHERE_SIZE);
}




void PrimitiveRenderer::buildAabb()
{
#define CUBE_NUM_VERT           8
#define CUBE_NUM_FACES          6
#define CUBE_NUM_EDGE_PER_FACE  4
#define CUBE_VERT_PER_OBJ       (CUBE_NUM_FACES*CUBE_NUM_EDGE_PER_FACE)
#define CUBE_VERT_ELEM_PER_OBJ  (CUBE_VERT_PER_OBJ*3)
#define CUBE_VERT_PER_OBJ_TRI   (CUBE_VERT_PER_OBJ+CUBE_NUM_FACES*2)    /* 2 extra edges per face when drawing quads as triangles */

	const int numFaces = 6;
	const int numEdgePerFace = 4;

	static chag::float3 cube_v[CUBE_NUM_VERT] =
	{
		{  .5f, .5f, .5f},
		{ -.5f, .5f, .5f},
		{ -.5f, -.5f, .5f},
		{ .5f, -.5f, .5f},
		{ .5f, -.5f, -.5f},
		{ .5f, .5f, -.5f},
		{ -.5f, .5f, -.5f},
		{ -.5f, -.5f, -.5f },
	};
	/* Normal Vectors */
	static chag::float3 cube_n[numFaces] =
	{
		{ 0.0f, 0.0f, 1.0f },
		{ 1.0f, 0.0f, 0.0f },
		{ 0.0f, 1.0f, 0.0f },
		{ -1.0f, 0.0f, 0.0f },
		{ 0.0f, -1.0f, 0.0f },
		{ 0.0f, 0.0f, -1.0f },
	};

	static GLubyte cube_vi[CUBE_VERT_PER_OBJ] =
	{
		0, 1, 2, 3,
		0, 3, 4, 5,
		0, 5, 6, 1,
		1, 6, 7, 2,
		7, 4, 3, 2,
		4, 7, 6, 5
	};
	const int numVerts = numFaces * numEdgePerFace;
	static chag::float3 vertOut[numVerts];
	static chag::float3 normOut[numVerts];
	static chag::float2 uvOut[numVerts];

	const int numInds = numFaces * (numEdgePerFace + 1);
	static int inds[numInds];
	glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
	for (int i = 0; i<numFaces; i++)
	{
		int normIdx = i;
		int faceIdxVertIdx = i*numEdgePerFace; /* index to first element of "row" in vertex indices */
		
		for (int j = 0; j < numEdgePerFace; j++)
		{
			int outIdx = i*numEdgePerFace + j;
			int vertIdx = cube_vi[faceIdxVertIdx + j];

			vertOut[outIdx] = cube_v[vertIdx];
			normOut[outIdx] = cube_n[normIdx];
			uvOut[outIdx] = chag::make_vector(float(j % 2), float((j / 2) % 2));

			inds[i*(numEdgePerFace + 1) + j] = outIdx;
		}
		inds[i*(numEdgePerFace + 1) + numEdgePerFace] = 0xffffffff;
	}

	g_aabbVerts.init(numVerts, vertOut);
	g_aabbNormals.init(numVerts, normOut);
	g_aabbUvs.init(numVerts, uvOut);
	g_aabbInds.init(numInds, inds);

	glGenVertexArrays(1, &g_aabbVaob);
	glBindVertexArray(g_aabbVaob);

	g_aabbInds.bind(GL_ELEMENT_ARRAY_BUFFER);

	g_aabbVerts.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	g_aabbNormals.bind();
	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	g_aabbUvs.bind();
	glVertexAttribPointer(AA_TexCoord, 2, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_TexCoord);
	g_aabbUvs.unbind();
	glBindVertexArray(0);
}



void PrimitiveRenderer::drawAabb(const chag::Aabb &aabb, SimpleShader *shader, bool wireFrame)
{
	glBindVertexArray(g_aabbVaob);

	chag::float4x4 tfm = chag::make_translation(aabb.getCentre()) * chag::make_scale<chag::float4x4>(aabb.getDiagonal());

	shader->setUniform("modelTransform", m_transform * tfm);
	glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
	CHECK_GL_ERROR();

	glDrawElements(wireFrame ? GL_LINE_LOOP : GL_TRIANGLE_FAN, GLsizei(g_aabbInds.size()), GL_UNSIGNED_INT, 0);
	CHECK_GL_ERROR();
	glBindVertexArray(0);
}




void PrimitiveRenderer::buildPlane()
{
	static chag::float3 plane_v[4] =
	{
		{1.0f, 1.0f, 0.0f  },
		{-1.0f, 1.0f, 0.0f },
		{-1.0f, -1.0f, 0.0f},
		{1.0f, -1.0f, 0.0f },
	};

	/* Normal Vectors */
	static chag::float3 plane_n[4] =
	{
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f },
	};
	static chag::float2 plane_uv[4] =
	{
		{ 1.0f, 1.0f },
		{ 0.0f, 1.0f },
		{ 0.0f, 0.0f },
		{ 1.0f, 0.0f },
	};

	g_planeVerts.init(4, plane_v);
	g_planeNormals.init(4, plane_n);
	g_planeUvs.init(4, plane_uv);

	glGenVertexArrays(1, &g_planeVaob);
	glBindVertexArray(g_planeVaob);

	g_planeVerts.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	g_planeNormals.bind();
	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	g_planeUvs.bind();
	glVertexAttribPointer(AA_TexCoord, 2, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_TexCoord);
	g_planeUvs.unbind();
	glBindVertexArray(0);
}



void PrimitiveRenderer::drawPlane(chag::float3 normal, chag::float3 point, chag::float3 alignToDir, float planeSize, SimpleShader *shader)
{
	normal = normalize(normal);
	chag::float3 biTangent = normalize(cross(alignToDir, normal));
	chag::float3 tangent = normalize(cross(biTangent, normal));

	glBindVertexArray(g_planeVaob);

	chag::float4x4 tfm = chag::make_matrix_from_zAxis(point, normal, biTangent) * chag::make_scale<chag::float4x4>(planeSize);
	shader->setUniform("modelTransform", m_transform * tfm);

	glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
}



void PrimitiveRenderer::drawPlane(chag::float3 normal, chag::float3 point, float planeSize, SimpleShader *shader)
{
	drawPlane(normal, point, perpendicular(normal), planeSize, shader);
}



void PrimitiveRenderer::buildQuadGrid()
{
	const int numPointsXY = s_maxQuadGrid + 1;
	std::vector<chag::float3> grid_v(numPointsXY * numPointsXY);
	std::vector<chag::float3> grid_n(numPointsXY * numPointsXY);
	std::vector<chag::float2> grid_uv(numPointsXY * numPointsXY);
	// identity rect, centered on origine in XY plane.
	for (int j = 0; j < numPointsXY; ++j)
	{
		for (int i = 0; i < numPointsXY; ++i)
		{
			chag::float2 uv = chag::make_vector(float(i), float(j)) / float(s_maxQuadGrid);
			grid_v[j * numPointsXY + i] = chag::make_vector3(uv * 2.0f - 1.0f, 0.0f);
			grid_n[j * numPointsXY + i] = chag::make_vector(0.0f, 0.0f, 1.0f);
			grid_uv[j * numPointsXY + i] = uv;
		}
	}

	// triangle list.
	std::vector<int> inds(s_maxQuadGrid * s_maxQuadGrid * 6);
	for (int j = 0; j < s_maxQuadGrid; ++j)
	{
		for (int i = 0; i < s_maxQuadGrid; ++i)
		{
			int *t = &inds[(j * s_maxQuadGrid + i) * 6];
			t[0] = (j * numPointsXY) + i;
			t[1] = (j * numPointsXY) + i + 1;
			t[2] = ((j + 1) * numPointsXY) + i;

			t[3] = t[2];// ((j + 1) * numPointsXY) + i;
			t[4] = t[1];// (j * numPointsXY) + i + 1;
			t[5] = ((j + 1) * numPointsXY) + i + 1;
		}
	}

	g_quadGridVerts.init(grid_v.size(), &grid_v[0]);
	g_quadGridNormals.init(grid_n.size(), &grid_n[0]);
	g_quadGridUvs.init(grid_uv.size(), &grid_uv[0]);
	g_quadGridInds.init(inds.size(), &inds[0]);

	glGenVertexArrays(1, &g_quadGridVaob);
	glBindVertexArray(g_quadGridVaob);

	g_quadGridInds.bind(GL_ELEMENT_ARRAY_BUFFER);

	g_quadGridVerts.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	g_quadGridNormals.bind();
	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	g_quadGridUvs.bind();
	glVertexAttribPointer(AA_TexCoord, 2, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_TexCoord);
	g_quadGridUvs.unbind();
	glBindVertexArray(0);
}



void PrimitiveRenderer::drawQuadGrid(chag::float3 normal, chag::float3 point, chag::float3 alignToDir, float planeSize, SimpleShader *shader)
{
	normal = normalize(normal);
	chag::float3 biTangent = normalize(cross(alignToDir, normal));
	chag::float3 tangent = normalize(cross(biTangent, normal));

	chag::float4x4 tfm = chag::make_matrix_from_zAxis(point, normal, biTangent) * chag::make_scale<chag::float4x4>(planeSize);
	shader->setUniform("modelTransform", m_transform * tfm);

	drawQuadGrid();
}



void PrimitiveRenderer::drawQuadGrid()
{
	glBindVertexArray(g_quadGridVaob);
	glDrawElements(GL_TRIANGLES, GLsizei(g_quadGridInds.size()), GL_UNSIGNED_INT, 0);
}



void PrimitiveRenderer::drawCone(const float3 &pos, const float3 &dir, float angle, float length, SimpleShader *shader, bool wireFrame)
{
	float xyScale = tanf(angle) * length;
	chag::float4x4 tfm = chag::make_matrix_from_zAxis(pos, dir, perpendicular(dir)) * chag::make_scale<chag::float4x4>(chag::make_vector(xyScale, xyScale, length));
	shader->setUniform("modelTransform", m_transform * tfm);

	glBindVertexArray(g_coneVaob);
	CHECK_GL_ERROR();
	glDrawElements(GL_TRIANGLES, GLsizei(g_coneInds.size()), GL_UNSIGNED_INT, 0);

	//glDrawElements(wireFrame ? GL_LINE_LOOP : GL_TRIANGLE_FAN, GLsizei(g_aabbInds.size()), GL_UNSIGNED_INT, 0);
	CHECK_GL_ERROR();
	glBindVertexArray(0);
}

const int CONE_SIZE = 24;

void PrimitiveRenderer::createConeMesh()
{
	using namespace chag;

	const int numVerts = CONE_SIZE * 3 + 1;

	chag::float3 pos[numVerts];
	chag::float3 norm[numVerts];
	chag::float2 uvs[numVerts];

	memset(uvs, 0, sizeof(uvs));

	// 1. the apex is at the origin & other end is for center of cap
	// pos[0:CONE_SIZE-1] = apex verts (same position, different normal)
	// pos[CONE_SIZE:2*CONE_SIZE-1] = base ring verts (different positions & normals as apex ones)
	// pos[2*CONE_SIZE:3*CONE_SIZE-1] = cap verts (same positions as above & nornals straight down)
	// pos[3*CONE_SIZE] = cap centre

	// cap centre
	pos[CONE_SIZE * 3] = chag::make_vector(0.0f, 0.0f, 1.0f);
	norm[CONE_SIZE * 3] = chag::make_vector(0.0f, 0.0f, 1.0f);

	// 2. create ring of verts at base -> circle in xy plane at z = 1.0f
	for (int i = 0; i < CONE_SIZE; ++i)
	{
		// ring aroung base first.
		float ang = g_pi * 2.0f * float(i) / float(CONE_SIZE);
		pos[CONE_SIZE + i] = make_vector(cosf(ang), sinf(ang), 1.0f);
		norm[CONE_SIZE + i] = normalize(make_vector(pos[CONE_SIZE + i].x, pos[CONE_SIZE + i].y, -1.0f));

		// apex position (origin), copy normals from base ring
		pos[i] = chag::make_vector3(0.0f);
		norm[i] = norm[CONE_SIZE + i];

		// cap verts
		pos[2 * CONE_SIZE + i] = pos[CONE_SIZE + i];
		norm[2 * CONE_SIZE + i] = chag::make_vector(0.0f, 0.0f, 1.0f);
	}
	// 3. create indexes connecting these things...
	const int numInds = CONE_SIZE * 2 * 3;
	static int inds[numInds];
	for (int i = 0; i < CONE_SIZE; ++i)
	{
		// cone side...
		inds[i * 3 + 0] = i; // apex
		inds[i * 3 + 1] = ((i + 1) % CONE_SIZE) + CONE_SIZE; // ring 1
		inds[i * 3 + 2] = i + CONE_SIZE; // ring 2

		// cap
		inds[CONE_SIZE * 3 + i * 3 + 0] = CONE_SIZE * 3; // cap center
		inds[CONE_SIZE * 3 + i * 3 + 1] = i + 2 * CONE_SIZE; // cap-ring 2
		inds[CONE_SIZE * 3 + i * 3 + 2] = ((i + 1) % CONE_SIZE) + 2 * CONE_SIZE; // cap-ring 1
	}


	g_coneVerts.init(numVerts, pos);
	g_coneNormals.init(numVerts, norm);
	g_coneUvs.init(numVerts, uvs);
	g_coneInds.init(numInds, inds);

	glGenVertexArrays(1, &g_coneVaob);
	glBindVertexArray(g_coneVaob);

	g_coneInds.bind(GL_ELEMENT_ARRAY_BUFFER);

	g_coneVerts.bind();
	glVertexAttribPointer(AA_Position, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Position);

	g_coneNormals.bind();
	glVertexAttribPointer(AA_Normal, 3, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_Normal);

	g_coneUvs.bind();
	glVertexAttribPointer(AA_TexCoord, 2, GL_FLOAT, false, 0, 0);
	glEnableVertexAttribArray(AA_TexCoord);
	g_coneUvs.unbind();
	glBindVertexArray(0);
}

}; // namespace chag


