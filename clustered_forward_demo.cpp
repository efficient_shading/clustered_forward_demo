/****************************************************************************/
/* Copyright (c) 2011, Ola Olsson
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
/****************************************************************************/
#include <GL/glew.h>

#if defined(_WIN32)
#include <GL/wglew.h>
#elif defined(__linux__)
#include <GL/glxew.h>
#endif // ~ platform

#include <GL/freeglut.h>

#include <IL/il.h>
#include <IL/ilu.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <algorithm>
#include <linmath/float4x4.h>
#include <linmath/int3.h>
#include <linmath/float3.h>
#include <utils/Rendering.h>
#include <utils/SimpleCamera.h>
#include <utils/PerformanceTimer.h>
#include <utils/CheckGLError.h>
#include <utils/GlBufferObject.h>
#include <utils/Random.h>
#include <utils/GLTimerQuery.h>
#include <utils/SimpleShader.h>
#include <utils/ComboShader.h>
#include <utils/CameraPoints.h>
#include <performance_monitor/profiler/Profiler.h>
#include <performance_monitor/PerfTreeBuilder.h>
#include "OBJModel.h"

#include "Light.h"
#include "LightGrid.h"
#include "CudaRenderer.h"
#include "utils/PrimitiveRenderer.h"

#undef near
#undef far

using namespace chag;

PrimitiveRenderer *g_primitiveRenderer = 0;

static OBJModel *g_model;

const int g_startWidth  = 1280;//1920;//256;//512;//800;//3840;//1024;//128;//60;//
const int g_startHeight = 720 ;//1080;//256;//512;//600;//2160;//1024;//128;//34;//

static int g_width  = g_startWidth;
static int g_height = g_startHeight;

static float g_near = 0.1f;
static float g_far = 10000.0f;
const float g_fov = 45.0f;
const chag::float3 g_ambientLight = { 0.05f, 0.05f, 0.05f };

#ifndef DISABLE_CUDA
static CudaRenderer *g_cudaRenderer = 0;
#endif // DISABLE_CUDA

static LightGrid g_lightGridOpaque;
static LightGrid g_lightGridTransparent;
static LightGrid g_debugGridOpaque;
static LightGrid g_debugGridTransparent;
static std::vector<Light> g_lights;
static SimpleCamera g_camera;
static PerformanceTimer g_appTimer;

static ComboShader *g_simpleShader = 0; 
static ComboShader *g_deferredShader = 0;
static ComboShader *g_tiledDeferredShader = 0;
static ComboShader *g_tiledForwardShader = 0;
static ComboShader *g_clusteringShader = 0; 
static ComboShader *g_clusteredForwardShader = 0; 
static SimpleShader *g_downSampleMinMaxShader = 0;
static SimpleShader *g_debugShader = 0;

// Note: GL_RGBA32F and 16x aa doesn't appear to be supported on less than Fermi, 
//       Not tested on high-end AMD cards.
static GLuint g_rgbaFpFormat = GL_RGBA16F;// GL_RGBA32F;// GL_R11F_G11F_B10F;//

static bool g_showLights = false; /**< Toggles drawing lights as additive spheres. */
static bool g_showLightGrid = false; /**< Toggles debug visualization of light grid, either tiles or clusters. */
static int g_showGBuffer = 0; /**< Cycles through G-buffers when using a deferred render method. */
static bool g_showInfo = false; /**< Toggles showing help text on screen. */
static bool g_showProfilerInfo = false; /**< Toggles showing profiler output on screen. */
static bool g_enablePreZ = true; /**< Toggles whether pre-z pass is performed. */
static bool g_enableDepthRangeTest = true; /**< Togles depth range test for tiled methods. */
static bool g_updateGridDebugData = true; /**< Toggles whether debug data related to the light grid is 
                                               updated, this allows freezing the light grid visualization 
                                               and flying around and inspecting from different angles */
static bool g_enableGpuClustering = true; /**< Controls whether the GPU is used to find required 
                                               clusters by performing a flag pass. Requires 
                                               GL_ARB_shader_image_load_store, and is automatically
                                               set to false if this extension is missing. */

static uint32_t g_numMsaaSamples = 1;
static uint32_t g_maxMsaaSamples = MAX_ALLOWED_MSAA_SAMPLES;

static bool g_mouseLookActive = false;
#if defined(__linux__)
static bool g_alternativeControls = true;
#else
static bool g_alternativeControls = false;
#endif // 

static bool g_pruneMaxZ = false;


static std::string g_sceneFileName = "house.obj";

static CameraPoints g_cameraPoints;

enum RenderMethod
{
	RM_TiledDeferred,
	RM_TiledForward,
	RM_Simple,
	RM_ClusteredForward,
	RM_Max,
};
static RenderMethod g_renderMethod = RM_ClusteredForward;
static const char *g_renderMethodNames[RM_Max] = 
{
	"TiledDeferred",
	"TiledForward",
	"Simple",
	"ClusteredForward",
};


enum TiledDeferredUniformBufferSlots
{
	TDUBS_Globals = OBJModel::UBS_Max, // use buffers that are not used by obj model.
	TDUBS_LightGrid,
	TDUBS_LightPositionsRanges,
	TDUBS_LightColors,
	TDUBS_Max,
};

enum TiledDeferredTextureUnits
{
	TDTU_LightIndexData = OBJModel::TU_Max, // avoid those used by objmodel.
	TDTU_Diffuse,
	TDTU_SpecularShininess,
	TDTU_Normal,
	TDTU_Ambient,
	TDTU_Depth,
	TDTU_Max,
};

static const char *g_tiledDeferredTextureUnitNames[TDTU_Max - TDTU_LightIndexData] =
{
	"tileLightIndexListsTex",
	"diffuseTex",
	"specularShininessTex",
	"normalTex",
	"ambientTex",
	"depthTex",
};

enum ClusteredForwardTextureUnits
{
	CFTU_LightIndexData = OBJModel::TU_Max, // avoid those used by objmodel.
	CFTU_GridData,
	CFTU_Max,
};

static const char *g_clusteredForwardTextureUnitNames[CFTU_Max - CFTU_LightIndexData] =
{
	"clusterLightIndexListsTex",
	"clusterGridTex",
};

enum DeferredRenderTargetIndices
{
  DRTI_Diffuse,
  DRTI_SpecularShininess,
  DRTI_Normal,
  DRTI_Ambient,
	DRTI_Depth,
  DRTI_Max,
};

// deferred render target.
static GLuint g_deferredFbo = 0;
static GLuint g_renderTargetTextures[DRTI_Max] = { 0 };
//GLuint g_depthTargetTexture = 0;
// forward MSAA render target (depth target is shared with deferred).
static GLuint g_forwardFbo = 0;
static GLuint g_forwardTargetTexture = 0;
//static GLuint g_forwardDepthTexture = 0;
// Contains depth min/max per tile, downsampled from frame buffer
static GLuint g_minMaxDepthFbo = 0;
static GLuint g_minMaxDepthTargetTexture = 0;
// Buffer texture that contains all the tile light lists.
static GLuint g_tileLightIndexListsTexture = 0;
static GlBufferObject<int> g_tileLightIndexListsBuffer;
// Buffers for grid and light data, bound to uniform blocks 
static GlBufferObject<int4> g_gridBuffer;
static GlBufferObject<float4> g_lightPositionRangeBuffer;
static GlBufferObject<float4> g_lightColorBuffer;

// Data used with clustered shading
static GLuint g_clusterFlagTexture = 0;
static GLuint g_clusterGridTexture = 0;
static GlBufferObject<uint32_t> g_clusterFlagBuffer;
static GlBufferObject<chag::uint2> g_clusterGridBuffer;
static GLuint g_clusterLightIndexListsTexture = 0;
static GlBufferObject<int> g_clusterLightIndexListsBuffer;
// Clustering debug data.
static std::vector<uint32_t> g_fullClusterFlagsDebug;
static chag::float4x4 g_prevProjection = chag::make_identity<chag::float4x4>();
static chag::float4x4 g_prevView = chag::make_identity<chag::float4x4>();


static struct ShaderGlobals_Std140
{
	chag::float4x4 viewMatrix; 
	chag::float4x4 viewProjectionMatrix; 
	chag::float4x4 inverseProjectionMatrix;
	chag::float4x4 normalMatrix; 
	chag::float3 worldUpDirection;
	float pad0;
	chag::float3 ambientGlobal;
	float pad1;
	chag::float2 invFbSize;
	chag::int2 fbSize;
	float recNear;
	float recLogSD1;
} g_shaderGlobals;


static GlBufferObject<ShaderGlobals_Std140> g_shaderGlobalsGl;

static void createFbos(int width, int height);
static void createShaders();
static void bindLightGridConstants(const LightGrid &lightGrid);
static void unbindLightGridConstants();
static void printInfo();
static void printPerformanceResultsToScreen();
static void printString(int x, int y, const char *fmt, ...);
static void checkFBO(uint32_t fbo);
static void downSampleDepthBuffer(std::vector<float2> &depthRanges);
static void updateLightBuffers(const Lights &lights);
static void bindClusteredForwardConstants();
static void initCuda();
static void deinitCuda();
static void printPerformanceResults();
static void assignLightsToClustersCpu(const Lights &lights, const chag::float4x4 &modelView, 
                                      const chag::float4x4 &projection, float near, 
                                      const std::vector<uint32_t> &fullClusterGrid);


static void nextRenderMethod()
{
	g_renderMethod = RenderMethod((g_renderMethod + 1) % RM_Max);
}



inline GLenum getRenderTargetTextureTargetType()
{
	return g_numMsaaSamples == 1 ? GL_TEXTURE_2D : GL_TEXTURE_2D_MULTISAMPLE;
}



static void printMemStats()
{
	if (GLEW_NVX_gpu_memory_info)
	{
		// this ensures whatever allocations we requested have actually happened...
		glFinish();
		int dedMem = 0;
		glGetIntegerv(GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX, &dedMem);
		int totMem = 0;
		glGetIntegerv(GL_GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX, &totMem);
		int currMem = 0;
		glGetIntegerv(GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX, &currMem);
		int evictCount = 0;
		glGetIntegerv(GL_GPU_MEMORY_INFO_EVICTION_COUNT_NVX, &evictCount);
		int evictMem = 0;
		glGetIntegerv(GL_GPU_MEMORY_INFO_EVICTED_MEMORY_NVX, &evictMem);

		printf("Dedicated: %0.2fGb, total %0.2fGb, avail: %0.2fGb, evict: %0.2fGb, evictCount: %d\n", float(dedMem) / (1024.0f * 1024.0f), float(totMem) / (1024.0f * 1024.0f), float(currMem) / (1024.0f * 1024.0f), float(evictMem) / (1024.0f * 1024.0f), evictCount);
	}
}


// helper to bind texture...
static void bindTexture(GLenum type, int texUnit, int textureId)
{
	glActiveTexture(GL_TEXTURE0 + texUnit);
	glBindTexture(type, textureId);
}



static float3 hueToRGB(float hue)
{
	const float s = hue * 6.0f;
	float r0 = clamp(s - 4.0f, 0.0f, 1.0f);
	float g0 = clamp(s - 0.0f, 0.0f, 1.0f);
	float b0 = clamp(s - 2.0f, 0.0f, 1.0f);

	float r1 = clamp(2.0f - s, 0.0f, 1.0f);
	float g1 = clamp(4.0f - s, 0.0f, 1.0f);
	float b1 = clamp(6.0f - s, 0.0f, 1.0f);

	// annoying that it wont quite vectorize...
	return make_vector(r0 + r1, g0 * g1, b0 * b1);
}



static void renderTiledDeferred(LightGrid &grid, const float4x4 &projectionMatrix)
{
	{
		glViewport(0, 0, g_width, g_height);
		CHECK_GL_ERROR();

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		glDepthMask(GL_FALSE);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		glBlendFunc(GL_ONE, GL_ONE);

		bindLightGridConstants(grid);

		g_tiledDeferredShader->begin(false);

		glBegin(GL_QUADS);
			glVertex2f(-1.0f, -1.0f);
			glVertex2f(1.0f, -1.0f);
			glVertex2f(1.0f, 1.0f);
			glVertex2f(-1.0f, 1.0f);
		glEnd();

		g_tiledDeferredShader->end();

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);

		unbindLightGridConstants();

		glDepthMask(GL_TRUE);

		glPopAttrib();
	}
}



static void updateShaderGlobals(const chag::float4x4 &viewMatrix, const chag::float4x4 &projectionMatrix, const chag::float3 &worldUpDirection, int width, int height, float near, float fov)
{
	g_shaderGlobals.viewMatrix = viewMatrix;
	g_shaderGlobals.viewProjectionMatrix = projectionMatrix * viewMatrix;
	g_shaderGlobals.inverseProjectionMatrix = inverse(projectionMatrix);
	g_shaderGlobals.normalMatrix = transpose(inverse(viewMatrix));
	g_shaderGlobals.worldUpDirection = worldUpDirection;
	g_shaderGlobals.ambientGlobal = g_ambientLight;
	g_shaderGlobals.invFbSize = make_vector(1.0f / float(width), 1.0f / float(height));
	g_shaderGlobals.fbSize = make_vector(width, height);

	g_shaderGlobals.recNear = 1.0f / near;
	uint32_t grid2dDimY = (height + LIGHT_GRID_TILE_DIM_Y - 1) / LIGHT_GRID_TILE_DIM_Y;
	float sD = 2.0f * tanf( toRadians(fov) * 0.5f ) / float(grid2dDimY);
	g_shaderGlobals.recLogSD1 = 1.0f / logf( sD + 1.0f );


	// copy to Gl
	g_shaderGlobalsGl.copyFromHost(&g_shaderGlobals, 1);
	// bind buffer.
	g_shaderGlobalsGl.bindSlot(GL_UNIFORM_BUFFER, TDUBS_Globals);
}



static void drawPreZPass()
{
	PROFILE_SCOPE_2("drawPreZPass", TT_OpenGl);
	glBindFramebuffer(GL_FRAMEBUFFER, g_forwardFbo);
	glViewport(0,0, g_width, g_height);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_DEPTH_BUFFER_BIT);
	if (g_numMsaaSamples != 1)
	{
		glEnable(GL_MULTISAMPLE);
	}
	glDepthFunc(GL_LEQUAL);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	g_simpleShader->begin(false);
	g_model->render(0, OBJModel::RF_Opaque);
	g_simpleShader->end();
	g_simpleShader->begin(true);
	g_model->render(0, OBJModel::RF_AlphaTested);
	g_simpleShader->end();

	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
}



static void glVertex(const float3 &v)
{
	glVertex3fv(&v.x);
}



static void glLoadMatrix(const chag::float4x4 &m)
{
	glLoadMatrixf(&m.c1.x);
}



static void glMultMatrix(const chag::float4x4 &m)
{
	glMultMatrixf(&m.c1.x);
}



static void drawAabb(const chag::Aabb &aabb)
{
	using namespace chag;

	chag::float3 mi = aabb.min;
	chag::float3 ma = aabb.max;

	// face 0 min X
	glVertex(mi);
	glVertex(make_vector(mi.x, ma.y, mi.z));
	glVertex(make_vector(ma.x, ma.y, mi.z));
	glVertex(make_vector(ma.x, mi.y, mi.z));
	// face 1, max X
	glVertex(make_vector(mi.x, mi.y, ma.z));
	glVertex(make_vector(ma.x, mi.y, ma.z));
	glVertex(ma);
	glVertex(make_vector(mi.x, ma.y, ma.z));

	// face 0 min Y
	glVertex(mi);
	glVertex(make_vector(ma.x, mi.y, mi.z));
	glVertex(make_vector(ma.x, mi.y, ma.z));
	glVertex(make_vector(mi.x, mi.y, ma.z));
	// face 1, max Y
	glVertex(make_vector(mi.x, ma.y, mi.z));
	glVertex(make_vector(mi.x, ma.y, ma.z));
	glVertex(ma);
	glVertex(make_vector(ma.x, ma.y, mi.z));

	// face 0 min Z
	glVertex(mi);
	glVertex(make_vector(mi.x, mi.y, ma.z));
	glVertex(make_vector(mi.x, ma.y, ma.z));
	glVertex(make_vector(mi.x, ma.y, mi.z));
	// face 1, max Z      
	glVertex(make_vector(ma.x, mi.y, mi.z));
	glVertex(make_vector(ma.x, ma.y, mi.z));
	glVertex(ma);
	glVertex(make_vector(ma.x, mi.y, ma.z));
}



static void onGlutDisplay()
{
	CHECK_GL_ERROR();
	{
		PROFILE_SCOPE_2("Frame", TT_OpenGl);

	PROFILE_COUNTER("Width", g_width);
	PROFILE_COUNTER("Height", g_height);

	CHECK_GL_ERROR();

	float4x4 projection = perspectiveMatrix(g_fov, float(g_width) / float(g_height), g_near, g_far);
	float4x4 modelView = lookAt(g_camera.getPosition(), g_camera.getPosition() + g_camera.getDirection(), g_camera.getUp());

	updateShaderGlobals(modelView, projection, g_camera.getWorldUp(), g_width, g_height, g_near, g_fov);

	glPushAttrib(GL_ALL_ATTRIB_BITS);
	switch(g_renderMethod)
	{
		case RM_TiledDeferred:
		{
			CHECK_GL_ERROR();
			// 0. pre-z pass

			if (g_enablePreZ)
			{
				drawPreZPass();
			}
			{
				PROFILE_SCOPE_2("deferredDraw", TT_OpenGl);
			// 1. deferred render model.
			CHECK_GL_ERROR();
			glBindFramebuffer(GL_FRAMEBUFFER, g_deferredFbo);

				CHECK_GL_ERROR();
				glViewport(0,0, g_width, g_height);
				CHECK_GL_ERROR();
				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				CHECK_GL_ERROR();
				glClear(GL_COLOR_BUFFER_BIT | (g_enablePreZ ? 0  : GL_DEPTH_BUFFER_BIT));
				CHECK_GL_ERROR();

				{
					PROFILE_SCOPE_2("Opaque", TT_OpenGl);
				g_model->render(g_deferredShader, OBJModel::RF_Opaque);
				}
				{
					PROFILE_SCOPE_2("AlphaTested", TT_OpenGl);
				g_model->render(g_deferredShader, OBJModel::RF_AlphaTested);
				}
			CHECK_GL_ERROR();
			}

			// 2. build grid
			std::vector<float2> depthRanges;
			if (g_enableDepthRangeTest)
			{
				downSampleDepthBuffer(depthRanges);
			}
			g_lightGridOpaque.build(
				make_vector<uint32_t>(LIGHT_GRID_TILE_DIM_X, LIGHT_GRID_TILE_DIM_Y), 
				make_vector<uint32_t>(g_width, g_height),
				g_lights,
				modelView,
				projection,
				g_near,
				depthRanges
				);

			{
				PROFILE_SCOPE_2("Shading", TT_OpenGl);
			// 3. apply tiled deferred lighting
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
				glViewport(0,0, g_width, g_height);
				glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
				glClear(GL_COLOR_BUFFER_BIT | (g_enablePreZ ? 0 : GL_DEPTH_BUFFER_BIT));

				renderTiledDeferred(g_lightGridOpaque, projection);
			}
		}
		break;
		case RM_TiledForward:
		{
			// 0. Pre-Z pass
			if (g_enablePreZ)
			{
				drawPreZPass();
			}

			// 1. build grid
			std::vector<float2> depthRanges;
			if (g_enableDepthRangeTest && g_enablePreZ)
			{
				downSampleDepthBuffer(depthRanges);
			}

			// First we build the grid without any depth range optimization.
			// This can be used for any geometry opaque as well as transparent, as it is simply a 2D structure.
			// This may be optimal for top-down applications, if depth complexity is limited for both geometry and lights.
			g_lightGridTransparent.build(
				make_vector<uint32_t>(LIGHT_GRID_TILE_DIM_X, LIGHT_GRID_TILE_DIM_Y), 
				make_vector<uint32_t>(g_width, g_height),
				g_lights,
				modelView,
				projection,
				g_near,
				std::vector<float2>()
				);

			{
				PROFILE_SCOPE_2("LightGridOpaque", TT_Cpu);
			// We take a copy of this, and prune the grid using depth ranges found from pre-z pass (for opaque geometry).
			// Note that the pruning does not occur if the pre-z pass was not performed (depthRanges is empty in this case).
			g_lightGridOpaque = g_lightGridTransparent;
			g_lightGridOpaque.prune(depthRanges);
			}

			{
				PROFILE_SCOPE_2("LightGridTransparent", TT_Cpu);
			// We, optionally, get rid of lights that are occluded by the opaque geometry by rejecting lights using the depth ranges
			// from the opaque pass, if present.
			if (g_pruneMaxZ)
			{
				g_lightGridTransparent.pruneFarOnly(g_near, depthRanges);
			}
			}
			CHECK_GL_ERROR();

			// 2. Render scene using light info from grid.
			bindLightGridConstants(g_lightGridOpaque);
			CHECK_GL_ERROR();
			if (g_numMsaaSamples != 1 || g_enablePreZ)
			{
				glBindFramebuffer(GL_FRAMEBUFFER, g_forwardFbo);
				if (g_numMsaaSamples != 1)
				{
					glEnable(GL_MULTISAMPLE);
				}
			}
			else
			{
				glBindFramebuffer(GL_FRAMEBUFFER, 0);
			}

			CHECK_GL_ERROR();
			glViewport(0,0, g_width, g_height);
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

			// if pre-z is enabled, we don't want to re-clear the frame buffer.
			glClear(GL_COLOR_BUFFER_BIT | (g_enablePreZ ? 0 : GL_DEPTH_BUFFER_BIT));
			
			CHECK_GL_ERROR();

			{
				PROFILE_SCOPE_2("Shading", TT_OpenGl);
			{
				PROFILE_SCOPE_2("Opaque", TT_OpenGl);
			g_model->render(g_tiledForwardShader, OBJModel::RF_Opaque);
			}
			{
				PROFILE_SCOPE_2("AlphaTested", TT_OpenGl);
			g_model->render(g_tiledForwardShader, OBJModel::RF_AlphaTested);
			}
			// draw set of transparent geometry, this time enabling blending.
			{
				PROFILE_SCOPE_2("Transparent", TT_OpenGl);
			bindLightGridConstants(g_lightGridTransparent);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			g_model->render(g_tiledForwardShader, OBJModel::RF_Transparent, modelView);
			glDisable(GL_BLEND);
			}
			}
			unbindLightGridConstants();
			CHECK_GL_ERROR();
			
			if (g_numMsaaSamples != 1 || g_enablePreZ)
			{
				PROFILE_SCOPE_2("MsaaBlit", TT_OpenGl);

				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				glBindFramebuffer(GL_READ_FRAMEBUFFER, g_forwardFbo);
				CHECK_GL_ERROR();
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
				glBlitFramebuffer(0, 0, g_width, g_height, 0, 0, g_width, g_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
				CHECK_GL_ERROR();
				glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
				CHECK_GL_ERROR();
			}
		}
		break;
		case RM_Simple:
		{
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			glViewport(0,0, g_width, g_height);
			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			g_simpleShader->begin(false);
			g_model->render(0, OBJModel::RF_Opaque);
			g_simpleShader->end();
			g_simpleShader->begin(true);
			g_model->render(0, OBJModel::RF_AlphaTested);
			g_simpleShader->end();
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			g_simpleShader->begin(true);
			g_model->render(0, OBJModel::RF_Transparent, modelView);
			g_simpleShader->end();
			glDisable(GL_BLEND);
		}
		break;
		case RM_ClusteredForward:
		{
			glDepthFunc(GL_LEQUAL);

			Lights viewSpaceLights(g_lights.size());
			for (size_t i = 0; i < g_lights.size(); ++i)
			{
				Light l = g_lights[i];
				l.position = transformPoint(modelView, l.position);
				viewSpaceLights[i] = l;
			}
			updateLightBuffers(viewSpaceLights);
			// 0. draw opaque using pre-z shader, to prep depth, without populating the grid.
			if (g_enablePreZ)
			{
				drawPreZPass();
			}
			CHECK_GL_ERROR();
			if (g_numMsaaSamples != 1 || g_enablePreZ)
			{
				glBindFramebuffer(GL_FRAMEBUFFER, g_forwardFbo);
				if (g_numMsaaSamples != 1)
				{
					glEnable(GL_MULTISAMPLE);
				}
			}
			else
			{
				glBindFramebuffer(GL_FRAMEBUFFER, 0);
			}

			CHECK_GL_ERROR();
			glViewport(0,0, g_width, g_height);
			glClearColor(0.4f, 0.3f, 0.2f, 1.0f);

			// if pre-z is enabled, we don't want to re-clear the frame buffer.
			glClear(GL_COLOR_BUFFER_BIT | (g_enablePreZ ? 0 : GL_DEPTH_BUFFER_BIT));

			bindTexture(GL_TEXTURE_BUFFER, CFTU_LightIndexData, 0 );
			bindTexture(GL_TEXTURE_BUFFER, CFTU_GridData, 0 );

			Globals globals;
			chag::float4x4 invProj = inverse(projection);
			chag::float4x4 invView = inverse(modelView);
			chag::float4x4 invViewProj = inverse(projection * modelView);
			float aspectRatio = float(g_width) / float(g_height);
			globals.update(&projection.c1.x, &invProj.c1.x, &modelView.c1.x, &invView.c1.x, &invViewProj.c1.x, g_width, g_height, g_numMsaaSamples, g_near, g_far, g_fov, aspectRatio);

			if (g_enableGpuClustering)
			{
				glBindImageTexture(0, g_clusterFlagTexture, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32UI);
				CHECK_GL_ERROR();
				// 1. draw everything using clustering shader (no color output).
				glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
				{
					PROFILE_SCOPE_2("Clustering", TT_OpenGl);
				//  1.1. draw opaque geometry, preps depth buffer (unless it's already prepped by pre z pass)
				g_clusteringShader->begin(false);
				g_model->render(0, OBJModel::RF_Opaque);
				g_clusteringShader->end();
				//  1.2. draw alpha tested geometry, 
				//   NOTE: depth mask is false, as layout(early_fragment_tests) in the shader may cause depth to be updated regardless of discard instruction.
				glDepthMask(GL_FALSE);
				g_clusteringShader->begin(true);
				g_model->render(0, OBJModel::RF_AlphaTested);
				g_clusteringShader->end();
				//  1.3. draw transparent, disable depth writes.
				g_clusteringShader->begin(true);
				g_model->render(0, OBJModel::RF_Transparent, modelView);
				g_clusteringShader->end();
				}
				glMemoryBarrier(GL_BUFFER_UPDATE_BARRIER_BIT);
				glDepthMask(GL_TRUE);
				glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);

				glBindImageTexture(0, 0, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_R32UI);

				// 
				if (g_updateGridDebugData)
				{
					ASSERT(g_fullClusterFlagsDebug.size() == g_clusterFlagBuffer.size());
					g_clusterFlagBuffer.copyToHost(&g_fullClusterFlagsDebug[0]);
				}
			// 2. build grid, assign lights etc.
#ifndef DISABLE_CUDA 
				if (g_cudaRenderer)
				{
					bindTexture(GL_TEXTURE_BUFFER, CFTU_LightIndexData, 0 );
					bindTexture(GL_TEXTURE_BUFFER, CFTU_GridData, 0 );

					g_cudaRenderer->buildClusters(globals, uint32_t(g_lights.size()));
				}
				else
#endif // DISABLE_CUDA
				{
					std::vector<uint32_t> fullClusterFlagsHost(g_clusterFlagBuffer.size());
					g_clusterFlagBuffer.copyToHost(&fullClusterFlagsHost[0]);
					assignLightsToClustersCpu(g_lights, modelView, projection, g_near, fullClusterFlagsHost);
					// clear buffer for next round.
					uint32_t *p = g_clusterFlagBuffer.beginMap();
					memset(p, 0, sizeof(p[0]) * LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y * LIGHT_GRID_MAX_DIM_Z);
					g_clusterFlagBuffer.endMap();

				}
			}
			else
			{
				// assign lights without info about which are needed
				assignLightsToClustersCpu(g_lights, modelView, projection, g_near, std::vector<uint32_t>());
			}
			bindClusteredForwardConstants();
			{
				PROFILE_SCOPE_2("Shading", TT_OpenGl);
			// 3. draw everything using shading shader.
			//  3.1. draw opaque geometry
			{
				PROFILE_SCOPE_2("Opaque", TT_OpenGl);
			g_model->render(g_clusteredForwardShader, OBJModel::RF_Opaque);
			}
			//  3.2. draw alpha tested geometry
			{
				PROFILE_SCOPE_2("AlphaTested", TT_OpenGl);
			g_model->render(g_clusteredForwardShader, OBJModel::RF_AlphaTested);
			}
			//  3.3. draw transparent, enable blending.
			{
				PROFILE_SCOPE_2("Transparent", TT_OpenGl);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			g_model->render(g_clusteredForwardShader, OBJModel::RF_Transparent, modelView);
			glDisable(GL_BLEND);
			CHECK_GL_ERROR();
			}
			}
			if (g_numMsaaSamples != 1 || g_enablePreZ)
			{
				PROFILE_SCOPE_2("MsaaBlit", TT_OpenGl);

				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				glBindFramebuffer(GL_READ_FRAMEBUFFER, g_forwardFbo);
				CHECK_GL_ERROR();
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
				glBlitFramebuffer(0, 0, g_width, g_height, 0, 0, g_width, g_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
				CHECK_GL_ERROR();
				glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
				glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
				CHECK_GL_ERROR();
			}
		};
		break;
	};
	glPopAttrib();

	/**
	 * Below this point follows debug visualization etc.
	 */

	if (g_showLights)
	{
		glPushAttrib(GL_ALL_ATTRIB_BITS);

		glMatrixMode(GL_PROJECTION);
		glLoadMatrix(projection);
		glMatrixMode(GL_MODELVIEW);
		glLoadMatrix(modelView);
		glDisable(GL_LIGHTING);
		glDepthMask(GL_FALSE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE);
		CHECK_GL_ERROR();

		g_debugShader->begin();
		g_debugShader->setUniform("viewProjectionTransform", projection * modelView);
		chag::float4x4 normalMat = chag::transpose(chag::inverse(modelView));
		g_debugShader->setUniform("normalMat", normalMat);

		for (size_t i = 0; i < g_lights.size(); ++i)
		{
			const Light &l = g_lights[i];
			g_debugShader->setUniform("color", chag::make_vector4(l.color, 0.1f));
			g_primitiveRenderer->drawSphere(l.position, l.range, g_debugShader);
		}
		CHECK_GL_ERROR();
		g_debugShader->end();

		glPopAttrib();
	}
	if (g_updateGridDebugData)
	{
		g_prevProjection = projection;
		g_prevView = modelView;
	}

	if (g_renderMethod == RM_ClusteredForward)
	{
		if (g_showLightGrid)
		{
			uint32_t grid2dDimY = (g_height + LIGHT_GRID_TILE_DIM_Y - 1) / LIGHT_GRID_TILE_DIM_Y;
			float sD = 2.0f * tanf(toRadians(g_fov) * 0.5f) / float(grid2dDimY);
			float zGridLocFar = logf(g_far / g_near)/logf(1.0f + sD);

			const chag::uint3 gridDim = chag::make_vector<uint32_t>((g_width + LIGHT_GRID_TILE_DIM_X - 1) / LIGHT_GRID_TILE_DIM_X, (g_height + LIGHT_GRID_TILE_DIM_Y - 1) / LIGHT_GRID_TILE_DIM_Y, uint32_t(ceilf(zGridLocFar) + 0.5f));

			glViewport(0, 0, g_width, g_height);
			glMatrixMode(GL_PROJECTION);
			glLoadMatrix(projection);
			glMatrixMode(GL_MODELVIEW);
			glLoadMatrix(modelView);

			float3 tileDimClip = { 2.0f * (LIGHT_GRID_TILE_DIM_X) / float(g_width), 2.0f * float(LIGHT_GRID_TILE_DIM_Y) / float(g_height), 1.0f };

			glPushMatrix();
			// tramsform back to world space so we can draw clip space aabbs.
			chag::float4x4 invPrevViewProj = chag::inverse(g_prevProjection * g_prevView);
			glMultMatrix(invPrevViewProj);

			glPushAttrib(GL_ALL_ATTRIB_BITS);
			
			glDisable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);
			glDepthMask(GL_FALSE);
			glEnable(GL_BLEND);
			glDisable(GL_TEXTURE_2D);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE);
			glDisable(GL_LIGHTING);

			static chag::float3 cols[] = 
			{
				make_vector(0.5f, 0.0f, 0.0f),
				make_vector(0.0f, 0.5f, 0.0f),
				make_vector(0.0f, 0.0f, 0.5f),
				make_vector(0.5f, 0.5f, 0.0f),
				make_vector(0.5f, 0.0f, 0.5f),
				make_vector(0.0f, 0.5f, 0.5f),
				make_vector(0.5f, 0.5f, 0.5f),
				make_vector(0.7f, 0.3f, 0.2f),
				make_vector(0.2f, 0.7f, 0.4f),
				make_vector(0.1f, 0.4f, 0.7f),
				make_vector(0.9f, 0.9f, 0.9f),
			};
			if (!g_fullClusterFlagsDebug.empty())
			{
				glBegin(GL_QUADS);
				for (int k = int(gridDim.z - 1); k >= 0; --k)
				{
					for (uint32_t j = 0; j < gridDim.y; ++j)
					{
						for (uint32_t i = 0; i < gridDim.x; ++i)
						{
							if (g_fullClusterFlagsDebug[(k * LIGHT_GRID_MAX_DIM_Y + j) * LIGHT_GRID_MAX_DIM_X + i])
							{
								chag::uint3 tileIndex = { i, j, k };

								chag::float3 tileMin = tileDimClip * chag::make_vector<float>(tileIndex) - chag::make_vector(1.0f, 1.0f, 1.0f);
								float viewTileNear = g_near * powf(1.0f + sD, float(tileIndex.z));
								chag::float4 tmp = g_prevProjection * make_vector(0.0f, 0.0f, -viewTileNear, 1.0f);        
								tileMin.z = tmp.z / tmp.w;
								chag::float3 tileMax = tileMin + tileDimClip;
								float viewTileFar = g_near * powf(1.0f + sD, float(tileIndex.z + 1));
								chag::float4 tmp2 = g_prevProjection * make_vector(0.0f, 0.0f, -viewTileFar, 1.0f);        
								tileMax.z = tmp2.z / tmp2.w;

								chag::float4 col = make_vector4(cols[((k * LIGHT_GRID_MAX_DIM_Y + j) * LIGHT_GRID_MAX_DIM_X + i) % (sizeof(cols) / sizeof(cols[0]))], 0.1f);
								glColor4fv(&col.x);
								drawAabb(make_aabb(tileMin, tileMax));
							}
						}
					}
				}
				glEnd();
			}

			glPopMatrix();
			glDepthMask(GL_TRUE);
			glPopAttrib();
			CHECK_GL_ERROR();
		}
	}
	else
	{
		if (g_updateGridDebugData)
		{
			g_debugGridOpaque = g_lightGridOpaque;
			g_debugGridTransparent = g_lightGridTransparent;
		}
		if (g_showLightGrid)
		{
			glViewport(0, 0, g_width, g_height);

			glPushMatrix();

			glMatrixMode(GL_PROJECTION);
			glLoadMatrix(projection);
			glMatrixMode(GL_MODELVIEW);
			glLoadMatrix(modelView);
			// tramsform back to world space so we can draw clip space aabbs.
			chag::float4x4 invPrevViewProj = chag::inverse(g_prevProjection * g_prevView);
			glMultMatrix(invPrevViewProj);

			glPushAttrib(GL_ALL_ATTRIB_BITS);

			glDisable(GL_DEPTH_TEST);
			glDisable(GL_CULL_FACE);
			glDepthMask(GL_FALSE);
			glEnable(GL_BLEND);
			glDisable(GL_TEXTURE_2D);
			glBlendFunc(GL_ONE, GL_ONE);
			glDisable(GL_LIGHTING);

			glBegin(GL_QUADS);
			const int border = 2;
			uint32_t offset = 0;
			const LightGrid &lightGrid = g_debugGridOpaque;
			const uint2 tileSize = lightGrid.getTileSize();
			float maxCount = float(lightGrid.getMaxTileLightCount());

			float2 tileDimClip = { 2.0f * (LIGHT_GRID_TILE_DIM_X) / float(g_width), 2.0f * float(LIGHT_GRID_TILE_DIM_Y) / float(g_height)};

			for (uint32_t y = 0; y < lightGrid.getGridDim().y; ++y)
			{
				for (uint32_t x = 0; x < lightGrid.getGridDim().x; ++x)
				{
					int count = lightGrid.tileLightCount(x, y);
					if (count)
					{
						chag::float2 tileMinMax = lightGrid.getTileMinMax(x,y);
						glColor3f(0.05f, 0.05f, 0.2f);

						if (tileMinMax.x == 0.0f)
						{
							tileMinMax.x = -g_near;
							tileMinMax.y = -g_far;
						}

						float zMin = ((g_near * g_far * 2.0f) / tileMinMax.x + (g_far + g_near)) / (g_far - g_near);
						float zMax = ((g_near * g_far * 2.0f) / tileMinMax.y + (g_far + g_near)) / (g_far - g_near);

						Aabb aabb;

						aabb.min.z = zMax;
						aabb.max.z = zMin;

						aabb.min.x = float(x) * tileDimClip.x - 1.0f;
						aabb.max.x = float(x + 1) * tileDimClip.x - 1.0f;
						aabb.min.y = float(y) * tileDimClip.y - 1.0f;
						aabb.max.y = float(y + 1) * tileDimClip.y - 1.0f;

						if (aabb.max.x > aabb.min.x)
						{
							drawAabb(aabb);
						}
					}
				}
			}

			glEnd();

			glDepthMask(GL_TRUE);
			glPopAttrib();
			glPopMatrix();
			CHECK_GL_ERROR();
		}
	}

	// blit gbuffer...
	if (g_showGBuffer != 0)
	{
		glBindFramebuffer(GL_READ_FRAMEBUFFER, g_deferredFbo);
		glReadBuffer(GL_COLOR_ATTACHMENT0 + g_showGBuffer - 1);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glBlitFramebuffer(0, 0, g_width, g_height, 0, 0, g_width, g_height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
		CHECK_GL_ERROR();
	}

	/**
	 * End of debug output section...
	 */

	} // end of profiler block.

	//printPerformanceResults();
	CHECK_GL_ERROR();

	printInfo();
	
	Profiler::instance().clearCommandBuffer();

	glutSwapBuffers();
}



static void onGlutIdle()
{
	float dt = float(g_appTimer.getElapsedTime());
	g_appTimer.restart();
	g_camera.update(dt);
	glutPostRedisplay();
}



static void saveCurrentCameraLocation()
{
	CameraPoints::Snap s;
	s.fwd = g_camera.getDirection();
	s.up = g_camera.getUp();
	s.pos = g_camera.getPosition();
	s.alternativeControls = g_camera.getAlternativeControls();
	s.moveVel = g_camera.getMoveVel();
	s.moveVelMult = g_camera.getMoveVelMult();

	g_cameraPoints.addSnap(s);
	g_cameraPoints.save();
}



static void useCurrentCameraLocation()
{
	CameraPoints::Snap s = g_cameraPoints.getCurrentSnap();
	g_camera.setTransform(make_matrix(make_matrix(cross(s.up, s.fwd), s.up, s.fwd), s.pos));
}



static std::vector<Light> generateLights(const Aabb &aabb, int num)
{
	std::vector<Light> result;
	// divide volume equally amongst lights
	const float lightVol = aabb.getVolume() / float(num);
	// set radius to be the cube root of volume for a light
	const float lightRad = pow(lightVol, 1.0f / 3.0f);
	// and allow some overlap
	const float maxRad = lightRad;// * 2.0f;
	const float minRad = lightRad;// / 2.0f;

	for (int i = 0; i < num; ++i)
	{
		float rad = randomRange(minRad, maxRad);
		float3 col = hueToRGB(randomUnitFloat()) * randomRange(0.4f, 0.7f);
		//float3 pos = { randomRange(aabb.min.x + rad, aabb.max.x - rad), randomRange(aabb.min.y + rad, aabb.max.y - rad), randomRange(aabb.min.z + rad, aabb.max.z - rad) };
		const float ind =  rad / 8.0f;
		float3 pos = { randomRange(aabb.min.x + ind, aabb.max.x - ind), randomRange(aabb.min.y + ind, aabb.max.y - ind), randomRange(aabb.min.z + ind, aabb.max.z - ind) };
		Light l = { pos, col, rad };

		result.push_back(l);
	}
	return result;
}



static void onGlutKeyboard(unsigned char key, int, int)
{
	g_camera.setMoveVelocity(length(g_model->getAabb().getHalfSize()) / 10.0f);

	if (g_camera.handleKeyInput(key, true))
	{
		return;
	}

	switch(tolower(key))
	{
	case 27: // esc
		glutLeaveMainLoop();
		break;
	case '\t':
		g_cameraPoints.nextSnap();
		useCurrentCameraLocation();
		// to remember current point.
		g_cameraPoints.save();
		break;
	case 127: // delete
		g_cameraPoints.removeSnap();
		g_cameraPoints.save();
		break;
	case 'r':
		g_lights = generateLights(g_model->getAabb(), int(g_lights.size()));
		break;
	case 't':
		g_showGBuffer = (g_showGBuffer + 1) % (DRTI_Depth + 1);
		break;
	case 'l':
		createShaders();
		break;
	case 'p':
		g_showLights = !g_showLights;
		break;
	case 'g':
		g_showLightGrid = !g_showLightGrid;
		break;
	case 'f':
		g_updateGridDebugData = !g_updateGridDebugData;
		break;
	case 'm':
		nextRenderMethod();
		break;
	case 'c':
		{
			g_numMsaaSamples <<= 1;
			if (g_numMsaaSamples > MAX_ALLOWED_MSAA_SAMPLES)
			{
				g_numMsaaSamples = 1;
			}
			// now we must recompile shaders, and re-create FBOs
			createShaders();
			createFbos(g_width, g_height);
		}
		break;
	case 'u':
		{
			float3 pos = g_camera.getPosition();
			static int upAxis = 0;
			static float3 ups[]  = { { 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f, 1.0f } };
			static float3 fwds[] = { { 0.0f, 1.0f, 0.0f }, { 0.0f, 0.0f, 1.0f }, { 1.0f, 0.0f, 0.0f } };
			g_camera.init(ups[upAxis], fwds[upAxis], pos);
			upAxis = (upAxis + 1) % 3;
		}
	case 'o':
		g_enablePreZ = !g_enablePreZ;
		break;
	case 'z':
		g_enableDepthRangeTest = !g_enableDepthRangeTest;
		break;
	case '+':
		{
			int newCount = min(int(NUM_POSSIBLE_LIGHTS), int(g_lights.size()) + NUM_POSSIBLE_LIGHTS / 8);
			g_lights = generateLights(g_model->getAabb(), newCount);
		}
		break;
	case '-':
		{
			int newCount = max(0, int(g_lights.size()) - NUM_POSSIBLE_LIGHTS / 8);
			g_lights = generateLights(g_model->getAabb(), newCount);
		}
		break;
	case 'k':
		saveCurrentCameraLocation();
		break;
	case 'h':
		g_pruneMaxZ = !g_pruneMaxZ;
		break;
	case 'b':
		break;
	}
}



static void onGlutKeyboardUp(unsigned char _key, int, int)
{
	if (g_camera.handleKeyInput(_key, false))
	{
		return;
	}
}



static void onGlutSpecial(int key, int, int)
{
	if (key == GLUT_KEY_F1)
	{
		g_showInfo = !g_showInfo;
	}
	if (key == GLUT_KEY_F2)
	{
		g_showProfilerInfo = !g_showProfilerInfo;
	}
}



void onGlutMouse(int button, int state, int x, int y)
{
	enum { EXTRA_GLUT_WHEEL_UP = 3, EXTRA_GLUT_WHEEL_DOWN = 4 };

	if( EXTRA_GLUT_WHEEL_UP == button && GLUT_UP == state )
	{
		g_camera.modulateMoveVelocity( 2.0f );
	}
	else if( EXTRA_GLUT_WHEEL_DOWN == button && GLUT_UP == state )
	{
		g_camera.modulateMoveVelocity( 0.5f );
	}

	if( !g_alternativeControls )
	{
		g_camera.resetMouseInputPosition();
	}
	else
	{
		if( GLUT_RIGHT_BUTTON == button && GLUT_UP == state )
		{
			g_camera.resetMouseInputPosition();
			g_mouseLookActive = !g_mouseLookActive;
		}
	}
}



void onGlutMotion(int x, int y)
{
	if( !g_alternativeControls )
	{
		g_camera.handleMouseInput(chag::make_vector(x,y));
	}

	if( g_mouseLookActive )
	{
		g_camera.handleMouseInput( chag::make_vector(x,y) );
	}
}



void onGlutMouseMotion( int x, int y )
{
	if( g_mouseLookActive )
	{
		g_camera.handleMouseInput( chag::make_vector(x,y) );
	}
}



static void onGlutReshape(int width, int height)
{
	if (g_width != width || g_height != height)
	{
		createFbos(width, height);
	}
	g_width = width;
	g_height = height;
 }



int main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	glutInitWindowSize(g_width, g_height);
	glutCreateWindow("Clustered Forward Shading Demo");

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	glutSwapBuffers();

	glewInit();

	printf("--------------------------------------\nOpenGL\n  Vendor: %s\n  Renderer: %s\n  Version: %s\n--------------------------------------\n", glGetString(GL_VENDOR), glGetString(GL_RENDERER), glGetString(GL_VERSION));

	if (!GLEW_VERSION_3_3)
	{
		fprintf(stderr, "This demo assumes that Open GL 3.3 is present.\n"
			"It may possibly run with less, so feel free to remove\n"
			"the check, and see what happens.\n");
		return 1;
	}

	if (!GL_ARB_shader_image_load_store)
	{
		printf("GL_ARB_shader_image_load_store, not present, required for Clustered Forward Shading *disabled*\n");
		g_enableGpuClustering = false;
	}

	int v = 0;
	glGetIntegerv(GL_MAX_UNIFORM_BLOCK_SIZE, &v);
	printf("GL_MAX_UNIFORM_BLOCK_SIZE: %d\n", v);
	glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &v);
	printf("GL_MAX_COLOR_TEXTURE_SAMPLES: %d\n", v);
	glGetIntegerv(GL_MAX_COLOR_TEXTURE_SAMPLES, &v);
	printf("GL_MAX_COLOR_TEXTURE_SAMPLES: %d\n", v);
	g_maxMsaaSamples = min<uint32_t>(g_maxMsaaSamples, MAX_ALLOWED_MSAA_SAMPLES);
	printf("--------------------------------------\n");

	g_primitiveRenderer = new PrimitiveRenderer;
	g_primitiveRenderer->init();

	ilInit();
	iluInit();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CCW);
	glDisable(GL_DITHER);
	glDisable(GL_ALPHA_TEST);
	
# if defined(_WIN32)
	wglSwapIntervalEXT(0);
# elif defined(__linux__)
	glXSwapIntervalSGI(0);
# endif // ~ platform

	glutDisplayFunc(onGlutDisplay);
	glutIdleFunc(onGlutIdle);
	glutKeyboardFunc(onGlutKeyboard);
	glutSpecialFunc(onGlutSpecial);
	glutKeyboardUpFunc(onGlutKeyboardUp);
	glutMouseFunc(onGlutMouse);
	glutPassiveMotionFunc(onGlutMouseMotion);
	glutMotionFunc(onGlutMotion);
	glutReshapeFunc(onGlutReshape);

	if (argc > 1)
	{
		g_sceneFileName = argv[1];
	}

	createFbos(g_width, g_height);
	
	g_gridBuffer.init(LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y, 0);

	g_lightPositionRangeBuffer.init(NUM_POSSIBLE_LIGHTS);
	g_lightColorBuffer.init(NUM_POSSIBLE_LIGHTS);

	g_clusterFlagBuffer.init(LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y * LIGHT_GRID_MAX_DIM_Z);
	g_clusterGridBuffer.init(LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y * LIGHT_GRID_MAX_DIM_Z);
	g_fullClusterFlagsDebug.resize(LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y * LIGHT_GRID_MAX_DIM_Z, 0);

	glGenTextures(1, &g_clusterFlagTexture);
	glBindTexture(GL_TEXTURE_BUFFER, g_clusterFlagTexture);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_R32UI, g_clusterFlagBuffer);

	glGenTextures(1, &g_clusterGridTexture);
	glBindTexture(GL_TEXTURE_BUFFER, g_clusterGridTexture);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_RG32UI, g_clusterGridBuffer);

	glGenTextures(1, &g_tileLightIndexListsTexture);
	// initial size is 1, because glTexBuffer failed if it was empty, we'll shovel in data later.
	g_tileLightIndexListsBuffer.init(1);
	glBindTexture(GL_TEXTURE_BUFFER, g_tileLightIndexListsTexture);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_R32I, g_tileLightIndexListsBuffer);
	glBindTexture(GL_TEXTURE_BUFFER, 0);
	CHECK_GL_ERROR();

	glGenTextures(1, &g_clusterLightIndexListsTexture);
	// initial size is 1, because glTexBuffer failed if it was empty, we'll shovel in data later.
	g_clusterLightIndexListsBuffer.init(4 * 1024 * 1024);
	glBindTexture(GL_TEXTURE_BUFFER, g_clusterLightIndexListsTexture);
	glTexBuffer(GL_TEXTURE_BUFFER, GL_R32I, g_clusterLightIndexListsBuffer);
	glBindTexture(GL_TEXTURE_BUFFER, 0);
	CHECK_GL_ERROR();
	g_shaderGlobalsGl.init(1, 0, GL_DYNAMIC_DRAW);

	initCuda();

	// clear the flag buffer for first time... (done here because CUDA barfs when mapped if done before...);
	uint32_t *p = g_clusterFlagBuffer.beginMap();
	memset(p, 0, sizeof(p[0]) * LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y * LIGHT_GRID_MAX_DIM_Z);
	g_clusterFlagBuffer.endMap();

	g_model = new OBJModel();
	if (!g_model->load(g_sceneFileName))
	{
		fprintf(stderr, "The file: '%s' could not be loaded.\n", g_sceneFileName.c_str());
		return 1;
	}	

	g_cameraPoints.load(g_sceneFileName + "_cameras.txt");

	if (g_cameraPoints.empty())
	{
		CameraPoints::Snap s;
		s.fwd = make_vector(0.0f, 0.0f, 1.0f);
		s.up = make_vector(0.0f, 1.0f, 0.0f);
		s.pos = make_vector(0.0f, 0.0f, 0.0f);
		s.alternativeControls = g_camera.getAlternativeControls();
		s.moveVel = g_camera.getMoveVel();
		s.moveVelMult = g_camera.getMoveVelMult();

		g_cameraPoints.addSnap(s);
		g_cameraPoints.save();
	}

	// note: shaders use the list of shading models which is discovered during model load, hence they must not be created before the model is loaded.
	createShaders();

	useCurrentCameraLocation();

	g_far = length(g_model->getAabb().getHalfSize()) * 3.0f;
	g_near = g_far / 1000.0f;

	g_lights = generateLights(g_model->getAabb(), NUM_POSSIBLE_LIGHTS);

	g_appTimer.start();
	glutMainLoop();

	deinitCuda();

	return 0;
}




// helper function to create and attach a frame buffer target object. 
static GLuint attachTargetTextureToFBO(GLuint fbo, GLenum attachment, int width, int height, GLenum internalFormat, GLenum format, GLenum type = GL_FLOAT, int msaaSamples = 0)
{
	GLuint targetTexture;
	glGenTextures(1, &targetTexture);

	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	CHECK_GL_ERROR();

	if (msaaSamples == 1)
	{
		glBindTexture(GL_TEXTURE_2D, targetTexture);
		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, format, type, NULL);

		CHECK_GL_ERROR();

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, targetTexture, 0);
		CHECK_GL_ERROR();

		glBindTexture(GL_TEXTURE_2D, 0);
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, targetTexture);
		CHECK_GL_ERROR();

		// if nvidias CSAA is available, we use it with half the color samples to coverage samples.
		if (GLEW_NV_texture_multisample)
		{
			glTexImage2DMultisampleCoverageNV( GL_TEXTURE_2D_MULTISAMPLE, msaaSamples, msaaSamples / 2, internalFormat, width, height, false );
		}
		else
		{
			glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, msaaSamples, internalFormat, width, height, false);
		}
		glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D_MULTISAMPLE, targetTexture, 0);

		CHECK_GL_ERROR();

		glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
	}
	glBindTexture(GL_TEXTURE_2D, 0);


	return targetTexture;
}




static void checkFBO(uint32_t fbo)
{
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		switch(glCheckFramebufferStatus(GL_FRAMEBUFFER))
		{
			case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
					printf("FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n");
					break;
			case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
					printf("FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n");
					break;
			case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
					printf("FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER\n");
					break;
			case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
					printf("FRAMEBUFFER_INCOMPLETE_READ_BUFFER\n");
					break;
			case GL_FRAMEBUFFER_UNSUPPORTED:
					printf("FRAMEBUFFER_UNSUPPORTED\n");
					break;
			default:
					printf("Unknown framebuffer problem %d\n", glCheckFramebufferStatus(GL_FRAMEBUFFER));
					break;
		}
		printf("Error: bad frame buffer config\n");
		DBG_BREAK();
		exit(-1);
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


static void deleteTextureIfUsed(GLuint texId)
{
	if (texId != 0)
	{
		glDeleteTextures(1, &texId);
	}
}

static void createFbos(int width, int height)
{
	int maxSamples = 0;
	glGetIntegerv(GL_MAX_SAMPLES, &maxSamples);

	for (int i = 0; i < DRTI_Max; ++i)
	{
		deleteTextureIfUsed(g_renderTargetTextures[i]);
	}

	// deferred render target
	if (!g_deferredFbo)
	{
		// only create if not already created.
		glGenFramebuffers(1, &g_deferredFbo);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, g_deferredFbo);
	for (int i = 0; i < DRTI_Depth; ++i)
	{
		g_renderTargetTextures[i] = attachTargetTextureToFBO(g_deferredFbo, GL_COLOR_ATTACHMENT0 + i, width, height, g_rgbaFpFormat, GL_RGBA, GL_FLOAT, g_numMsaaSamples);
	}
	g_renderTargetTextures[DRTI_Depth] = attachTargetTextureToFBO(g_deferredFbo, GL_DEPTH_ATTACHMENT, width, height, GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT, GL_FLOAT, g_numMsaaSamples);

	CHECK_GL_ERROR();
	GLenum bufs[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
	glDrawBuffers(sizeof(bufs) / sizeof(bufs[0]), bufs);
	glReadBuffer(GL_NONE); 
	

	checkFBO(g_deferredFbo);

	// forward shading render target
	if (!g_forwardFbo)
	{
		// only create if not already created.
		glGenFramebuffers(1, &g_forwardFbo);
	}
	deleteTextureIfUsed(g_forwardTargetTexture);

	glBindFramebuffer(GL_FRAMEBUFFER, g_forwardFbo);
	g_forwardTargetTexture = attachTargetTextureToFBO(g_forwardFbo, GL_COLOR_ATTACHMENT0, width, height, GL_RGBA, GL_RGBA, GL_FLOAT, g_numMsaaSamples);
	// Shared with deferred
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, getRenderTargetTextureTargetType(), g_renderTargetTextures[DRTI_Depth], 0);


	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glReadBuffer(GL_COLOR_ATTACHMENT0); 
	
	checkFBO(g_forwardFbo);

	if (!g_minMaxDepthFbo)
	{
		// only create if not already created.
		glGenFramebuffers(1, &g_minMaxDepthFbo);
	}
	deleteTextureIfUsed(g_minMaxDepthTargetTexture);

	glBindFramebuffer(GL_FRAMEBUFFER, g_minMaxDepthFbo);

	uint2 tileSize = make_vector<uint32_t>(LIGHT_GRID_TILE_DIM_X, LIGHT_GRID_TILE_DIM_Y);
	uint2 resolution = make_vector<uint32_t>(width, height);
	uint2 gridRes = (resolution + tileSize - 1) / tileSize;

	g_minMaxDepthTargetTexture = attachTargetTextureToFBO(g_minMaxDepthFbo, GL_COLOR_ATTACHMENT0, gridRes.x, gridRes.y, GL_RG32F, GL_RGBA, GL_FLOAT, 1);

	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glReadBuffer(GL_COLOR_ATTACHMENT0); 
	
	checkFBO(g_minMaxDepthFbo);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	CHECK_GL_ERROR();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	CHECK_GL_ERROR();

	glBindTexture(GL_TEXTURE_2D, 0);

	printMemStats();
}

static void bindObjModelAttributes(ComboShader *shader)
{
	shader->bindAttribLocation(OBJModel::AA_Position, "position");
	shader->bindAttribLocation(OBJModel::AA_Normal, "normalIn");
	shader->bindAttribLocation(OBJModel::AA_TexCoord, "texCoordIn");
	shader->bindAttribLocation(OBJModel::AA_Tangent, "tangentIn");
	shader->bindAttribLocation(OBJModel::AA_Bitangent, "bitangentIn");
}

static void setObjModelUniformBindings(ComboShader *shader)
{
	shader->begin(false);
		shader->setUniform("diffuse_texture", OBJModel::TU_Diffuse);
		shader->setUniform("opacity_texture", OBJModel::TU_Opacity);
		shader->setUniform("specular_texture", OBJModel::TU_Specular);
		shader->setUniform("normal_texture", OBJModel::TU_Normal);
	shader->end();
	shader->setUniformBufferSlot("MaterialProperties", OBJModel::UBS_MaterialProperties);
 
	shader->setUniformBufferSlot("Globals", TDUBS_Globals);
}


static void setTiledLightingUniformBindings(ComboShader *shader)
{
	shader->setUniformBufferSlot("Globals", TDUBS_Globals);
	shader->setUniformBufferSlot("LightGrid", TDUBS_LightGrid);
	shader->setUniformBufferSlot("LightPositionsRanges", TDUBS_LightPositionsRanges);
	shader->setUniformBufferSlot("LightColors", TDUBS_LightColors);

	shader->begin(false);

	for (int i = TDTU_LightIndexData; i < TDTU_Max; ++i)
	{
		shader->setUniform(g_tiledDeferredTextureUnitNames[i - TDTU_LightIndexData], i);
	}
	shader->end();
}


static void setClusteredForwardLightingUniformBindings(ComboShader *shader)
{
	shader->setUniformBufferSlot("Globals", TDUBS_Globals);
	shader->setUniformBufferSlot("LightPositionsRanges", TDUBS_LightPositionsRanges);
	shader->setUniformBufferSlot("LightColors", TDUBS_LightColors);

	shader->begin(false);

	for (int i = CFTU_LightIndexData; i < CFTU_Max; ++i)
	{
		shader->setUniform(g_clusteredForwardTextureUnitNames[i - CFTU_LightIndexData], i);
	}
	shader->end();
}

template <typename T>
static void deleteIfThere(T *&shader)
{
	if (shader)
	{
		delete shader;
		shader = 0;
	}
}

static void createShaders()
{
	SimpleShader::Context shaderCtx;
	shaderCtx.setPreprocDef("NUM_POSSIBLE_LIGHTS", NUM_POSSIBLE_LIGHTS);
	shaderCtx.setPreprocDef("LIGHT_GRID_TILE_DIM_X", LIGHT_GRID_TILE_DIM_X);
	shaderCtx.setPreprocDef("LIGHT_GRID_TILE_DIM_Y", LIGHT_GRID_TILE_DIM_Y);
	shaderCtx.setPreprocDef("LIGHT_GRID_MAX_DIM_X", LIGHT_GRID_MAX_DIM_X);
	shaderCtx.setPreprocDef("LIGHT_GRID_MAX_DIM_Y", LIGHT_GRID_MAX_DIM_Y);
	shaderCtx.setPreprocDef("LIGHT_GRID_MAX_DIM_Z", LIGHT_GRID_MAX_DIM_Z);
	shaderCtx.setPreprocDef("NUM_MSAA_SAMPLES", int(g_numMsaaSamples));

	deleteIfThere(g_simpleShader);
	g_simpleShader = new ComboShader("shaders/simple_vertex.glsl", "shaders/simple_fragment.glsl", shaderCtx);
		bindObjModelAttributes(g_simpleShader);
		g_simpleShader->bindFragDataLocation(0, "resultColor");
	g_simpleShader->link();
	setObjModelUniformBindings(g_simpleShader);


	// deferred shader
	deleteIfThere(g_deferredShader);
	g_deferredShader = new ComboShader("shaders/deferred_vertex.glsl", "shaders/deferred_fragment.glsl", shaderCtx, g_model->getShadingModels());
		bindObjModelAttributes(g_deferredShader);

		g_deferredShader->bindFragDataLocation(DRTI_Diffuse, "outDiffuse");
		g_deferredShader->bindFragDataLocation(DRTI_SpecularShininess, "outSpecularShininess");
		g_deferredShader->bindFragDataLocation(DRTI_Normal, "outNormal");
		g_deferredShader->bindFragDataLocation(DRTI_Ambient, "outAmbient");
	g_deferredShader->link();
	setObjModelUniformBindings(g_deferredShader);

	// tiled deferred shader
	deleteIfThere(g_tiledDeferredShader);
	g_tiledDeferredShader = new ComboShader("shaders/tiled_deferred_vertex.glsl", "shaders/tiled_deferred_fragment.glsl", shaderCtx);
		g_tiledDeferredShader->bindFragDataLocation(0, "resultColor");
	g_tiledDeferredShader->link();
	setTiledLightingUniformBindings(g_tiledDeferredShader);

	// tiled forward shader
	deleteIfThere(g_tiledForwardShader);
	g_tiledForwardShader = new ComboShader("shaders/tiled_forward_vertex.glsl", "shaders/tiled_forward_fragment.glsl", shaderCtx, g_model->getShadingModels());
		bindObjModelAttributes(g_tiledForwardShader);
		g_tiledForwardShader->bindFragDataLocation(0, "resultColor");
	g_tiledForwardShader->link();
	setObjModelUniformBindings(g_tiledForwardShader);
	setTiledLightingUniformBindings(g_tiledForwardShader);

	// downsample min/max shader
	deleteIfThere(g_downSampleMinMaxShader);
	g_downSampleMinMaxShader = new SimpleShader("shaders/tiled_deferred_vertex.glsl", "shaders/downsample_minmax_fragment.glsl", shaderCtx);
		g_downSampleMinMaxShader->bindFragDataLocation(0, "resultMinMax");
	g_downSampleMinMaxShader->link();
	g_downSampleMinMaxShader->setUniformBufferSlot("Globals", TDUBS_Globals);

	if (g_enableGpuClustering)
	{
		// clustering forward shader
		deleteIfThere(g_clusteringShader);
		g_clusteringShader = new ComboShader("shaders/clustering_vertex.glsl", "shaders/clustering_fragment.glsl", shaderCtx);
			bindObjModelAttributes(g_clusteringShader);
			g_clusteringShader->bindFragDataLocation(0, "resultColor");
		g_clusteringShader->link();
		setObjModelUniformBindings(g_clusteringShader);
		g_clusteringShader->begin(false);
		g_clusteringShader->setUniform("fullClusterImage", 0);
		g_clusteringShader->end();
	}
	{
		// clustered forward shader
		deleteIfThere(g_clusteredForwardShader);
		g_clusteredForwardShader = new ComboShader("shaders/clustered_forward_vertex.glsl", "shaders/clustered_forward_fragment.glsl", shaderCtx, g_model->getShadingModels());
			bindObjModelAttributes(g_clusteredForwardShader);
			g_clusteredForwardShader->bindFragDataLocation(0, "resultColor");
		g_clusteredForwardShader->link();
		setObjModelUniformBindings(g_clusteredForwardShader);
		setClusteredForwardLightingUniformBindings(g_clusteredForwardShader);
	}

	deleteIfThere(g_debugShader);
	g_debugShader = new SimpleShader("shaders/debug_vertex.glsl", "shaders/debug_fragment.glsl", shaderCtx);
	g_debugShader->bindFragDataLocation(0, "resultColor");
	g_debugShader->link();
	g_debugShader->setUniformBufferSlot("Globals", TDUBS_Globals);
	g_debugShader->begin();
	g_debugShader->setUniform("modelTransform", chag::make_identity<chag::float4x4>());
	g_debugShader->end();
}



static void updateLightBuffers(const Lights &lights)
{
	const size_t maxLights = NUM_POSSIBLE_LIGHTS;
	static float4 light_position_range[maxLights];
	static float4 light_color[maxLights];

	memset(light_position_range, 0, sizeof(light_position_range));
	memset(light_color, 0, sizeof(light_color));

	for (size_t i = 0; i < std::min(maxLights, lights.size()); ++i)
	{
		light_position_range[i] = make_vector4(lights[i].position, lights[i].range);
		light_color[i] = make_vector4(lights[i].color, 1.0f);
	}
	g_lightPositionRangeBuffer.copyFromHost(light_position_range, NUM_POSSIBLE_LIGHTS);
	g_lightColorBuffer.copyFromHost(light_color, NUM_POSSIBLE_LIGHTS);
}

static void bindLightGridConstants(const LightGrid &lightGrid)
{
	// pack grid data in int4 because this will work on AMD GPUs, where constant registers are 4-vectors.
	static chag::int4 tmp[LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y];
	{
		const int *counts = lightGrid.tileCountsDataPtr();
		const int *offsets = lightGrid.tileDataPtr();
		for (int i = 0; i < LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y; ++i)
		{
			tmp[i] = chag::make_vector(counts[i], offsets[i], 0, 0);
		}
	}
	g_gridBuffer.copyFromHost(tmp, LIGHT_GRID_MAX_DIM_X * LIGHT_GRID_MAX_DIM_Y);

	if (lightGrid.getTotalTileLightIndexListLength())
	{
		g_tileLightIndexListsBuffer.copyFromHost(lightGrid.tileLightIndexListsPtr(), lightGrid.getTotalTileLightIndexListLength());
		// This should not be neccessary, but for amd it seems to be (HD3200 integrated)
		glBindTexture(GL_TEXTURE_BUFFER, g_tileLightIndexListsTexture);
		glTexBuffer(GL_TEXTURE_BUFFER, GL_R32I, g_tileLightIndexListsBuffer);
		CHECK_GL_ERROR();
	}
	bindTexture(GL_TEXTURE_BUFFER, TDTU_LightIndexData, g_tileLightIndexListsTexture);

	updateLightBuffers(lightGrid.getViewSpaceLights());

	g_gridBuffer.bindSlot(GL_UNIFORM_BUFFER, TDUBS_LightGrid);
	g_lightPositionRangeBuffer.bindSlot(GL_UNIFORM_BUFFER, TDUBS_LightPositionsRanges);
	g_lightColorBuffer.bindSlot(GL_UNIFORM_BUFFER, TDUBS_LightColors);

	for (int i = 0; i < DRTI_Max; ++i)
	{
		bindTexture(getRenderTargetTextureTargetType(), i + TDTU_Diffuse, g_renderTargetTextures[i]);
	}
}

static void unbindLightGridConstants()
{
	glBindTexture(GL_TEXTURE_BUFFER, 0);
	bindTexture(GL_TEXTURE_BUFFER, TDTU_LightIndexData, 0);

	for (int i = 0; i < DRTI_Max; ++i)
	{
		bindTexture(getRenderTargetTextureTargetType(), i + TDTU_Diffuse, 0);
	}
}



static void bindClusteredForwardConstants()
{
	//std::vector<int> balh(10 * 1024 * 1024, 0);
	//g_clusterLightIndexListsBuffer.copyFromHost(&balh[0], balh.size());
	//glBindTexture(GL_TEXTURE_BUFFER, g_clusterLightIndexListsTexture);
	//glTexBuffer(GL_TEXTURE_BUFFER, GL_R32I, g_clusterLightIndexListsBuffer);
	//glBindTexture(GL_TEXTURE_BUFFER, 0);

	bindTexture(GL_TEXTURE_BUFFER, CFTU_LightIndexData, g_clusterLightIndexListsTexture);
	bindTexture(GL_TEXTURE_BUFFER, CFTU_GridData, g_clusterGridTexture);

	g_lightPositionRangeBuffer.bindSlot(GL_UNIFORM_BUFFER, TDUBS_LightPositionsRanges);
	g_lightColorBuffer.bindSlot(GL_UNIFORM_BUFFER, TDUBS_LightColors);
}


static void printString(int x, int y, const char *fmt, ...)
{
	char text[256];
	va_list		ap;
	va_start(ap, fmt);
	vsnprintf( text, 256, fmt, ap );
	va_end(ap);

	glRasterPos2i(x, y);
	for(size_t i = 0; i < strlen(text); ++i)
	{
		glutBitmapCharacter(GLUT_BITMAP_8_BY_13, text[i]); 
	}
}


static void printInfo()
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);

	glViewport(0, 0, g_width, g_height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, float(g_width), float(g_height), 0.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDepthMask(GL_FALSE);
	glDisable(GL_BLEND);
	glDisable(GL_LIGHTING);

	glDisable(GL_TEXTURE_2D);

	if (g_showInfo)
	{
		int yOffset = 10;
		int yStep = 13;
		glColor3f(1.0f, 1.0f, 1.0f);
		printString(10, yOffset, "Toggle info: <F1>");
		printString(10, yOffset += yStep, "Method ('m'): %s", g_renderMethodNames[g_renderMethod]);
		char msaaBuffer[64];
		sprintf(msaaBuffer, "%dx", g_numMsaaSamples);
		printString(10, yOffset += yStep, "MSAA Level ('c'): %s (Max: %dx)", g_numMsaaSamples == 1 ? "Off" : msaaBuffer, g_maxMsaaSamples);
		printString(10, yOffset += yStep, "Show Lights ('p'): %s", g_showLights ? "On" : "Off");
		printString(10, yOffset += yStep, "Show Light Grid ('g'): %s", g_showLightGrid ? "On" : "Off");
		printString(10, yOffset += yStep, "Update Light Grid Debug Data ('f'): %s", g_updateGridDebugData ? "On" : "Off");
		printString(10, yOffset += yStep, "Toggle PreZ ('o'): %s", g_enablePreZ ? "On" : "Off");
		printString(10, yOffset += yStep, "Toggle PruneMaxZ ('h'): %s", g_pruneMaxZ ? (g_enablePreZ ? "On" : "Off (Requires PreZ!)") : "Off");
		printString(10, yOffset += yStep, "Toggle Depth Range Test ('z'): %s", g_enableDepthRangeTest ? "On" : "Off");
		printString(10, yOffset += yStep, "Add/Remove %d Lights '+/-'", NUM_POSSIBLE_LIGHTS / 8);
		const char *gBufferNames[] = 
		{
			"Off",
			"Diffuse",
			"SpecularShininess",
			"Normal",
			"Ambient",
		};
		printString(10, yOffset += yStep, "Shown G-Buffer ('t'): %s", gBufferNames[g_showGBuffer]);
		printString(10, yOffset += yStep, "Re-load Shaders ('l')");
		printString(10, yOffset += yStep, "Cycle Up direction ('u')");
		printString(10, yOffset += yStep, "Scene file name: %s", g_sceneFileName.c_str());
		printString(10, yOffset += yStep, "G-Buffer Format: %s", g_rgbaFpFormat == GL_RGBA16F ? "GL_RGBA16F" : (g_rgbaFpFormat == GL_RGBA32F ? "GL_RGBA32F" : "Unknown"));
		printString(10, yOffset += yStep, "Num Lights: %d", int(g_lights.size()));
	}
	else
	{
		glColor3f(0.4f, 0.4f, 0.4f);
		printString(10, 10, "Toggle info: <F1>");
	}

	if (g_showProfilerInfo)
	{
		glColor3f(1.0f, 1.0f, 1.0f);
		printString(g_width / 2, 10, "Toggle profiler info: <F2>");
		printPerformanceResultsToScreen();
	}
	else
	{
		glColor3f(0.4f, 0.4f, 0.4f);
		printString(g_width / 2, 10, "Toggle profiler info: <F2>");
	}

	glPopAttrib();
}


static void downSampleDepthBuffer(std::vector<float2> &depthRanges)
{
	PROFILE_SCOPE_2("downSampleDepthBuffer", TT_OpenGl);
	glBindFramebuffer(GL_FRAMEBUFFER, g_minMaxDepthFbo);

		uint2 tileSize = make_vector<uint32_t>(LIGHT_GRID_TILE_DIM_X, LIGHT_GRID_TILE_DIM_Y);
		uint2 resolution = make_vector<uint32_t>(g_width, g_height);
		uint2 gridRes = (resolution + tileSize - 1) / tileSize;
	{
		PROFILE_SCOPE_2("Shader", TT_OpenGl);
	
		glViewport(0, 0, gridRes.x, gridRes.y);
		glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		CHECK_GL_ERROR();

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		
		glPushAttrib(GL_ALL_ATTRIB_BITS);
		glDepthMask(GL_FALSE);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);

		g_downSampleMinMaxShader->begin();
		if (g_numMsaaSamples == 1)
		{
			g_downSampleMinMaxShader->setTexture2D("depthTex", g_renderTargetTextures[DRTI_Depth], 0);
		}
		else
		{
			g_downSampleMinMaxShader->setTexture2DMS("depthTex", g_renderTargetTextures[DRTI_Depth], 0);
		}

		glBegin(GL_QUADS);
			glVertex2f(-1.0f, -1.0f);
			glVertex2f(1.0f, -1.0f);
			glVertex2f(1.0f, 1.0f);
			glVertex2f(-1.0f, 1.0f);
		glEnd();
		CHECK_GL_ERROR();

		g_downSampleMinMaxShader->end();

		CHECK_GL_ERROR();
		glDepthMask(GL_TRUE);
		glPopAttrib();
		CHECK_GL_ERROR();
	}
	{
		PROFILE_SCOPE_2("ReadBack", TT_Cpu);
		glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
		depthRanges.resize(gridRes.x * gridRes.y);
		glReadPixels(0, 0, gridRes.x, gridRes.y, GL_RG, GL_FLOAT, &depthRanges[0]);
		CHECK_GL_ERROR();
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}



static void initCuda()
{
	// Ensure profiler does not try to use cuda timers if cuda is not initialized/avaliable
	Profiler::setEnabledTimers(Profiler::TT_Cpu | Profiler::TT_OpenGl);

#ifndef DISABLE_CUDA
	g_cudaRenderer = CudaRenderer::create();
	if (g_cudaRenderer)
	{
		g_cudaRenderer->init(g_clusterFlagBuffer, g_clusterGridBuffer, g_lightPositionRangeBuffer, g_lightColorBuffer, g_clusterLightIndexListsBuffer);
	}
#endif // DISABLE_CUDA
}



static void deinitCuda()
{
#ifndef DISABLE_CUDA
	if (g_cudaRenderer)
	{
		delete g_cudaRenderer;
		g_cudaRenderer = 0;
	}
#endif // DISABLE_CUDA
}



struct ScreenRect3D
{
	chag::uint3 min;
	chag::uint3 max;
	uint32_t index;
};



float calcClusterZ(float viewSpaceZ, float recNear, float recLogSD1)
{
	float gridLocZ = logf(-viewSpaceZ * recNear) * recLogSD1;

	return gridLocZ;
}



static void buildRects3D(const chag::uint2 resolution, const Lights &lights, const chag::float4x4 &modelView, const chag::float4x4 &projection, float near, std::vector<ScreenRect3D> &rects)
{
	PROFILE_SCOPE_2("BuildRects", TT_Cpu);

	rects.clear();

	float recNear = 1.0f / near;
	uint32_t grid2dDimY = (resolution.y + LIGHT_GRID_TILE_DIM_Y - 1) / LIGHT_GRID_TILE_DIM_Y;
	float sD = 2.0f * tanf( toRadians(g_fov) * 0.5f ) / float(grid2dDimY);
	float recLogSD1 = 1.0f / logf( sD + 1.0f );

	for (uint32_t i = 0; i < lights.size(); ++i)
	{
		const Light &l = lights[i];
		chag::float3 vp = transformPoint(modelView, l.position);
		LightGrid::ScreenRect rect = LightGrid::findScreenSpaceBounds(projection, vp, l.range, resolution.x, resolution.y, near);

		if (rect.min.x < rect.max.x && rect.min.y < rect.max.y)
		{
			ScreenRect3D r3;
			r3.index = i;

			uint32_t zMin = uint32_t(max(0.0f, calcClusterZ(vp.z + l.range, recNear, recLogSD1)));
			uint32_t zMax = uint32_t(max(0.0f, ceilf(calcClusterZ(vp.z - l.range, recNear, recLogSD1)) + 0.5f));

			const uint2 tileSize = { LIGHT_GRID_TILE_DIM_X, LIGHT_GRID_TILE_DIM_Y };

			r3.min = make_vector3(rect.min / tileSize, zMin);
			r3.max = make_vector3((rect.max + tileSize - 1) / tileSize, zMax);
			rects.push_back(r3);
		}
	}
	PROFILE_COUNTER("NumVisibleLights", int(rects.size()));
}

#define STORE_LIGHT_INDS_FWD 0

/**
 * Build clusters with ALL that have any lights affecting them, i.e. a 3D scan convert of the light bounds.
 * This is fairly costly both in terms of storage and CPU cycles, as most clusters do not contain any geometry.
 * This is intended as a fallback for the clustered forward algorithm to run on non DX11 GPUs. It is also
 * used as a fallback on non NVIDIA GPUS to replace CUDA, in this case it uses cluster flags to only assign
 * lights where there is actual geometry, in this case it can be quite fast.
 * It also shows what the worst case (i.e. all clusters have some geometry in them) storage requirements.
 */
static void assignLightsToClustersCpu(const Lights &lights, const chag::float4x4 &modelView, const chag::float4x4 &projection, float near, 
                                 const std::vector<uint32_t> &fullClusterGrid)
{
	using namespace chag;
	PROFILE_SCOPE_2("LightGridBuild", TT_Cpu);

	const uint32_t *gridFlags = fullClusterGrid.empty() ? 0 : &fullClusterGrid[0];

	const uint2 tileSize = { LIGHT_GRID_TILE_DIM_X, LIGHT_GRID_TILE_DIM_Y };

	uint32_t grid2dDimY = (g_height + LIGHT_GRID_TILE_DIM_Y - 1) / LIGHT_GRID_TILE_DIM_Y;
	float sD = 2.0f * tanf(toRadians(g_fov) * 0.5f) / float(grid2dDimY);
	float zGridLocFar = logf(g_far / g_near)/logf(1.0f + sD);

	const chag::uint3 gridDim = chag::make_vector<uint32_t>((g_width + LIGHT_GRID_TILE_DIM_X - 1) / LIGHT_GRID_TILE_DIM_X, (g_height + LIGHT_GRID_TILE_DIM_Y - 1) / LIGHT_GRID_TILE_DIM_Y, uint32_t(ceilf(zGridLocFar) + 0.5f));

	PROFILE_COUNTER("GridDimX", gridDim.x);
	PROFILE_COUNTER("GridDimY", gridDim.y);
	PROFILE_COUNTER("GridDimZ", gridDim.z);
	
	uint32_t maxTileLightCount = 0;

	uint2 resolution = { g_width, g_height };
	std::vector<ScreenRect3D> rects;
	buildRects3D(resolution, lights, modelView, projection, near, rects);


	std::vector<uint2> gridHost(g_clusterGridBuffer.size(), make_vector(0u, 0u));
	uint2 *grid = &gridHost[0];

	// Note: It may be better to map the buffer, but apparently not on my machine (tm)
	//g_clusterGridBuffer.beginMap(true);
	//memset(grid, 0, sizeof(grid[0]) * g_clusterGridBuffer.size());
	
#define GRID_OFFSETS(_x_,_y_,_z_) (grid[(_z_ * LIGHT_GRID_MAX_DIM_Y + _y_) * LIGHT_GRID_MAX_DIM_X + _x_].x)
#define GRID_COUNTS(_x_,_y_,_z_) (grid[(_z_ * LIGHT_GRID_MAX_DIM_Y + _y_) * LIGHT_GRID_MAX_DIM_X + _x_].y)
#define GRID_FLAGS(_x_,_y_,_z_) (gridFlags[(_z_ * LIGHT_GRID_MAX_DIM_Y + _y_) * LIGHT_GRID_MAX_DIM_X + _x_])

	uint32_t totalus = 0u;
	{  
	PROFILE_SCOPE_2("Pass1", TT_Cpu);
	for (size_t i = 0; i < rects.size(); ++i)
	{
		ScreenRect3D r = rects[i];

		chag::uint3 l = clamp(r.min, make_vector(0U,0U,0U), gridDim);
		chag::uint3 u = clamp(r.max, make_vector(0U,0U,0U), gridDim);

		for (uint32_t z = l.z; z < u.z; ++z)
		{
			for (uint32_t y = l.y; y < u.y; ++y)
			{
				for (uint32_t x = l.x; x < u.x; ++x)
				{
					if (!gridFlags || GRID_FLAGS(x, y, z))
					{
						GRID_COUNTS(x, y, z) += 1;
						++totalus;
					}
				}
			}
		}
	}
	}

	PROFILE_COUNTER("GridDataCount", int(totalus));

	uint32_t offset = 0;
	{  
	PROFILE_SCOPE_2("BuildOffsets", TT_Cpu);
	for (uint32_t z = 0; z < gridDim.z; ++z)
	{
		for (uint32_t y = 0; y < gridDim.y; ++y)
		{
			for (uint32_t x = 0; x < gridDim.x; ++x)
			{
				uint32_t count = GRID_COUNTS(x,y, z);
#if STORE_LIGHT_INDS_FWD
				GRID_OFFSETS(x, y, z) = offset;
				GRID_COUNTS(x,y, z) = 0;
#else // STORE_LIGHT_INDS_FWD
				// set offset to be just past end, then decrement while filling in
				GRID_OFFSETS(x, y, z) = offset + count;
#endif // STORE_LIGHT_INDS_FWD
				offset += count;

				// for debug/profiling etc.
				maxTileLightCount = chag::max(maxTileLightCount, count);
			}
		}
	}
	PROFILE_COUNTER("MaxTileLightCount", int(maxTileLightCount));
	}
	if (totalus && rects.size())
	{
		PROFILE_SCOPE_2("Pass2", TT_Cpu);
		//int *data = g_clusterLightIndexListsBuffer.beginMap(true);
		std::vector<int> indexesHost(totalus);
		int *data = &indexesHost[0];
		for (size_t i = 0; i < rects.size(); ++i)
		{
			ScreenRect3D r = rects[i];

			chag::uint3 l = clamp(r.min, make_vector(0U,0U,0U), gridDim);
			chag::uint3 u = clamp(r.max, make_vector(0U,0U,0U), gridDim);

			for (uint32_t z = l.z; z < u.z; ++z)
			{
				for (uint32_t y = l.y; y < u.y; ++y)
				{
					for (uint32_t x = l.x; x < u.x; ++x)
					{
						if (!gridFlags || GRID_FLAGS(x, y, z))
						{
#if STORE_LIGHT_INDS_FWD
							uint32_t count = GRID_COUNTS(x,y, z);
							GRID_COUNTS(x,y, z) = count + 1;
							uint32_t offset = GRID_OFFSETS(x, y, z) + count;
							data[offset] = r.index;
#else // !STORE_LIGHT_INDS_FWD
							// store reversely into next free slot
							uint32_t offset = GRID_OFFSETS(x, y, z) - 1;
							data[offset] = r.index;
							GRID_OFFSETS(x, y, z) = offset;
#endif // STORE_LIGHT_INDS_FWD
						}
					}
				}
			}
		}
		{
			PROFILE_SCOPE_2("copyLightIndexListsFromHost", TT_Cpu);
			//g_clusterLightIndexListsBuffer.endMap();
			g_clusterLightIndexListsBuffer.copyFromHost(&indexesHost[0], indexesHost.size());
		}
	}
	{
		PROFILE_SCOPE_2("copyGridFromHost", TT_Cpu);
	//g_clusterGridBuffer.endMap();
	g_clusterGridBuffer.copyFromHost(&gridHost[0], gridHost.size());
	}
#undef GRID_COUNTS
#undef GRID_OFFSETS
#undef GRID_FLAGS
}



// Below lives code that decodes the stream of tokens generated by the profiler and builds a tree out of them,
// and this is then used to output performance figures on-screen and/or to console.
// Yes, it does leak memory, but this leaking is bounded, so, well.

static PerfTreeBuilder g_perfTreeBuilder;


static void printNodeList(FILE *f, const std::vector<PerfTreeNode*> &nl, uint32_t indent = 0)
{
	for (std::vector<PerfTreeNode*>::const_iterator it = nl.begin(); it != nl.end(); ++it)
	{
		fprintf(f, "%*s%-25s: %8.2f %8I64d\n", indent * 2, "", (*it)->label.c_str(), 1000.0 * (*it)->time, (*it)->count);
		printNodeList(f, (*it)->children, indent + 1);
	}
}



static void printPerformanceResults()
{
	FILE *f = stdout;
	fprintf(f, "****************************************************************\n");

	std::vector<PerfTreeNode*> roots = g_perfTreeBuilder.build(Profiler::instance());

	printNodeList(f, roots);
	fprintf(f, "****************************************************************\n");
	fclose(f);
}



static void screenPrintNodeList(const std::vector<PerfTreeNode*> &nl, int &yOffset, int yStep, uint32_t indent = 0)
{
	for (std::vector<PerfTreeNode*>::const_iterator it = nl.begin(); it != nl.end(); ++it)
	{
		printString(indent * 20 + g_width / 2, yOffset, "%-25s: %8.2f %8.2f %8I64d", (*it)->label.c_str(), 1000.0 * (*it)->time, 1000.0 * (*it)->average, (*it)->count);
		yOffset += yStep;
		screenPrintNodeList((*it)->children, yOffset, yStep, indent + 1);
	}
}



static void printPerformanceResultsToScreen()
{
	std::vector<PerfTreeNode*> roots = g_perfTreeBuilder.build(Profiler::instance());

	int yOffset = 23;
	int yStep = 13;
	screenPrintNodeList(roots, yOffset, yStep);
}

